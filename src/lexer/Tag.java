package lexer;

import java.util.ArrayList;
import java.util.Arrays;

public class Tag {
	public final static int EOF = -1, identifier = -2, number = -3,
			keywords = -4, True = -5, False = -6, string = -7, Char = -9,
			type = -10, OP = -11;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	static ArrayList<String> Type = new ArrayList(Arrays.asList("void", "char",
			"int", "struct", "union"));

	@SuppressWarnings({ "unchecked", "rawtypes" })
	static ArrayList<String> Keywords = new ArrayList(Arrays.asList("if",
			"else", "while", "for", "continue", "break", "return", 
			"malloc", "printf","getchar"));

	public static boolean indexOfType(String str) {
		return Type.indexOf(str.intern()) != -1;
	}

	public static boolean indexOfKeywords(String str) {
		return Keywords.indexOf(str.intern()) != -1;
	}
}
