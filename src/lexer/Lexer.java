package lexer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.Reader;

import error.LexicalError;

public class Lexer {
	File file;
	Reader reader;

	public Lexer(String filename) {
		file = new File(filename);
		try {
			reader = new InputStreamReader(new FileInputStream(file));
			ThisChar = (char) getChar();
			NextChar = (char) getChar();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Lexer() {
		reader = new InputStreamReader(System.in);
		ThisChar = (char) getChar();
		NextChar = (char) getChar();
	}

	int res = 0;

	int getChar() {
		if (res != -1) {
			try {
				if ((res = reader.read()) == -1) {
					return 3;
				} else {
					return res;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 3;
	}

	void getNext() {
		LastChar = ThisChar;
		ThisChar = NextChar;
		NextChar = (char) getChar();		
		if (ThisChar == '\n') lineNumber+=1;
		if(ThisChar == '\\' && (NextChar == '\n' || NextChar == '\r')) {
			if(NextChar == '\r') getNext();
			getNext();
			getNext();
		}
	}

	char LastChar = ' ', ThisChar = ' ', NextChar = ' ';

	public String IdentifierStr, StringStr, OpStr;
	public int NumVal;
	public char CharVal;
	public int lineNumber=1;
	public int currentTag;

	public void getTok() throws LexicalError {
		currentTag = getToken();
	}
	
	@SuppressWarnings("deprecation")
	int getToken() throws LexicalError {
		boolean flag = false;
		while(!flag) {					
			flag = true;
			while (Character.isSpace(ThisChar))
				getNext();
			if(ThisChar == 3) return Tag.EOF;
			// comment "//"
			if ((ThisChar == '/' && NextChar == '/') || ThisChar == '#') {
				flag = false;
				while (true) {
					getNext();
					if (LastChar == '\n' || LastChar == 3)
						break;
				}
				if(LastChar == 3) return Tag.EOF;
			}
	
			// Comment "/* */"
			if (ThisChar == '/' && NextChar == '*') {
				flag = false;
				getNext(); // eat '/'
				getNext(); // eat '*'
				while (true) {
					getNext();
					if (LastChar == '*' && ThisChar == '/' || ThisChar == 3) {
						getNext();
						break;
					}
				}
				if(LastChar == 3) return Tag.EOF;
			}
		}

		while (Character.isSpace(ThisChar))
			getNext();

		if (Character.isDigit(ThisChar)) {
			int radix;
			String NumStr = String.valueOf(ThisChar);
			if (ThisChar == '0') {
				if (NextChar == 'x' || NextChar == 'X' ) {
					radix = 16;
					getNext();
				} else if (Character.isDigit(NextChar)) {
					radix = 8;
				} else
					radix = 10;
			} else {
				radix = 10;				
			}
			getNext();
			while(true) {
				if(radix == 8 && !isOctal(ThisChar)) break;
				if(radix == 16 && !isHex(ThisChar)) break;
				if(radix == 10 && !Character.isDigit(ThisChar))break;
				NumStr += String.valueOf(ThisChar);
				getNext();
			} 
			NumVal = Integer.parseInt(NumStr, radix);
			return Tag.number;
		}

		if (Character.isLetter(ThisChar) || ThisChar == '_' || ThisChar == '$') {
			IdentifierStr = "";
			do {
				IdentifierStr += String.valueOf(ThisChar);
				getNext();
			} while (Character.isLetterOrDigit(ThisChar) || ThisChar == '_' || ThisChar == '$');

			if (Tag.indexOfType(IdentifierStr))
				return Tag.type;

			if (Tag.indexOfKeywords(IdentifierStr))
				return Tag.keywords;
			
			if (IdentifierStr.intern() == "sizeof") {
				OpStr = "sizeof";
				return Tag.OP;
			}			

			return Tag.identifier;
		}

		if (ThisChar == '"') {
			StringStr = "";
			getNext();
			while (true) {				
				if (ThisChar == '\n')
					throw new LexicalError("A string can't contain a enter");
				if (ThisChar == '"') {
					getNext();
					while(Character.isSpace(ThisChar))getNext();
					if(ThisChar != '"') break;
				} else	{
					StringStr += String.valueOf(readChar());
				}
			}
			return Tag.string;
		}

		if (ThisChar == '\'') {
			getNext();
			do {
				CharVal = readChar();
			} while (ThisChar != '\'');
			getNext();
			return Tag.Char;
		}

		if (ThisChar == '|' || ThisChar == '&' || ThisChar == '=') {
			OpStr = String.valueOf(ThisChar);
			getNext();
			if (ThisChar == LastChar || ThisChar == '=') {
				OpStr += String.valueOf(ThisChar);
				getNext();
			}
			return Tag.OP;
		}

		if (ThisChar == '<' || ThisChar == '>') {
			OpStr = String.valueOf(ThisChar);
			getNext();
			if (ThisChar == LastChar) {
				OpStr += String.valueOf(ThisChar);
				getNext();
				if (ThisChar == '=') {
					OpStr += String.valueOf(ThisChar);
					getNext();
				}
			} else if (ThisChar == '=') {
				OpStr += String.valueOf(ThisChar);
				getNext();
			}
			return Tag.OP;
		}

		if (ThisChar == '+') {
			OpStr = String.valueOf(ThisChar);
			getNext();
			if (ThisChar == '+') {
				OpStr += String.valueOf(ThisChar);
				getNext();
			} else	if (ThisChar == '=') {
				OpStr += String.valueOf(ThisChar);
				getNext();
			}
			return Tag.OP;
		}

		if (ThisChar == '-') {
			OpStr = String.valueOf(ThisChar);
			getNext();
			if (ThisChar == '-') {
				OpStr += String.valueOf(ThisChar);
				getNext();
			}
			if (ThisChar == '=' || ThisChar == '>') {
				OpStr += String.valueOf(ThisChar);
				getNext();
			}
			return Tag.OP;
		}

		if (NextChar == '='
				&& (ThisChar == '!' || ThisChar == '*' || ThisChar == '/'
						|| ThisChar == '%' || ThisChar == '^')) {
			OpStr = String.valueOf(ThisChar);
			OpStr += String.valueOf(NextChar);
			getNext();
			getNext();
			return Tag.OP;
		}

		if (ThisChar == (char) 3)
			return Tag.EOF;

		OpStr = String.valueOf(ThisChar);
		getNext();
		return Tag.OP;
	}

	private char readChar() throws LexicalError {
		if (ThisChar == '\\') {
			getNext();
			if (ThisChar == 'x') {
				String Str="0x";
				getNext();
				if(!isHex(ThisChar))throw new LexicalError("\\x used with invalid hex digits ");
				Str+=String.valueOf(ThisChar);
				getNext();
				if(!isHex(ThisChar))throw new LexicalError("\\x used with invalid hex digits ");
				Str+=String.valueOf(ThisChar);
				int num = Integer.parseInt(Str, 16);
				getNext();
				return (char)num;
			}
			if (Character.isDigit(ThisChar)) {
				String Str=String.valueOf(ThisChar);
				getNext();
				if(!isOctal(ThisChar)) {
					int num = Integer.parseInt(Str, 8);
					return (char)num;
				}
				Str += String.valueOf(ThisChar);
				getNext();
				if(!isOctal(ThisChar)) {
					int num = Integer.parseInt(Str, 8);
					return (char)num;
				}
				Str += String.valueOf(ThisChar);
				getNext();
				int num = Integer.parseInt(Str, 8);
				return (char)num;
			}
			char res;
			switch (ThisChar) {
			case 'a':
				res = 7;
				break;
			case 'b':
				res = 8;
				break;
			case 'f':
				res = 12;
				break;
			case 'n':
				res = 10;
				break;
			case 'r':
				res = 13;
				break;
			case 't':
				res = 9;
				break;
			case 'v':
				res = 11;
				break;
			case '\\':
				res = 92;
				break;
			case '\'':
				res = 39;
				break;
			case '\"':
				res = 34;
				break;
			default:
				throw new LexicalError(String.format("uncongnize character: \\%c",ThisChar));
			}
			getNext();
			return res;
		} else {
			char res = ThisChar;
			getNext();
			return res;
		}
	}

	private boolean isOctal(char ch) {
		return ch >= '0' && ch <= '8';
	}

	private boolean isHex(char ch) {
		return Character.isDigit(ch) || (ch >= 'A' && ch <= 'F') || (ch >= 'a' && ch <= 'f');
	}
}
