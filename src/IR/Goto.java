package IR;

public class Goto extends Instruction {
	Label label;
	public Goto(Label label) {
		this.label = label;
	}
	public String toString() {
		return "Goto "+label.toString();
	}
	public Label getLabel() {
		return label;
	}
	@Override
	public Name x() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Name y() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Name z() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int u() {
		// TODO Auto-generated method stub
		return 0;
	}
}