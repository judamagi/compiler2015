package IR;

import java.io.IOException;

import Env.IREnv;
import Env.StaticData;
import MIPS.MIPSWithSimpleGeneration;
import MIPS.Register;
import parser.Array;
import parser.Pointer;
import parser.Record;
import parser.Type;

abstract public class Name implements Comparable<Name>{
	//public abstract Type getType();
	public String allocateValue(String register, IREnv nowEnv, char CH,int offset) throws IOException {
		if(this instanceof Constant) {
			if(this instanceof CharConstant) {
				MIPSWithSimpleGeneration.gen(String.format("\t li %s, %d", register,((CharConstant)this).getValue()));
			}
			if(this instanceof IntegerConstant) {
				MIPSWithSimpleGeneration.gen(String.format("\t li %s, %d", register,((IntegerConstant)this).getValue()));
			}
			if(this instanceof __String__) {
				if(offset == 0 ) {
					MIPSWithSimpleGeneration.gen(String.format("\t la %s, %s", register,((__String__)this).getValue()));
				} else {
					MIPSWithSimpleGeneration.gen(String.format("\t la %s, %s+%d", register,((__String__)this).getValue(),offset));
				}
			}
			return register;
		} else {
			if(this.isRecordOrArray()) CH = 'a';
			if(this.isArray() && ((Variable)this).isParam()) CH = 'w';
			String res;
			if(MIPSWithSimpleGeneration.assignTable.containsKey(this)) {
				Register cd = MIPSWithSimpleGeneration.assignTable.get(this);
				if(cd.getConserve() == this)return cd.toString();
				//if(!(cd.getConserve() instanceof Temp))MIPSWithSimpleGeneration.gen(String.format("\t sw %s, %d($sp)", cd.toString(),nowEnv.getOffset(cd.getConserve())));
				cd.setConserve((Variable) this);
				register = cd.toString();
			}
			if(((Variable)this).isGlobal()) {
				if(StaticData.isGlobalString(((Variable)this).globalAddress())) CH = 'a';
				if(offset == 0) {
					res =  String.format("\t l%c %s, %s", CH, register,((Variable)this).globalAddress());
				}else {
					res =  String.format("\t l%c %s, %s+%d", CH, register,((Variable)this).globalAddress(),offset);
				}
			} else {
				res = String.format("\t l%c %s, %d($sp)", CH, register,offset+nowEnv.getOffset((Variable)this),offset);
			}
			MIPSWithSimpleGeneration.gen(res);
			return register;
		}
	}
	
	public boolean isRecordOrArray() {
		if(this instanceof Constant || this instanceof Temp) return false;
		return ((Variable)this).isRecordOrArray();
	}

	public String storeValue(String register, IREnv nowEnv, char CH,int offset) throws Exception {
		if(this instanceof Variable) {
			if(MIPSWithSimpleGeneration.assignTable.containsKey(this)){
				Register cd = MIPSWithSimpleGeneration.assignTable.get(this);
				/*if(cd.getConserve()==this && !(this instanceof Temp)) {
					cd.setConserve(null);
					return String.format("\t sw %s, %d($sp)", cd.toString(),nowEnv.getOffset((Variable) this));
				}*/
				cd.setConserve(null);
			}
			if(((Variable)this).isGlobal()) {
				return String.format("\t s%c %s, %s+%d", CH,register,((Variable)this).globalAddress(),offset);
			} else {
				return String.format("\t s%c %s, %d($sp)", CH,register,offset+nowEnv.getOffset((Variable)this));
			}
		} else {
			throw new Exception("error storeValue");
		}
	}
	
	public String allocateAddress(String register, IREnv nowEnv, int offset) throws Exception {
		if(this instanceof Variable) {
			if(((Variable)this).isGlobal()) {
				return String.format("\t la %s, %s+%d", register,((Variable)this).globalAddress(),offset);
			} else {
				return String.format("\t la %s, %d($sp)", register,offset+nowEnv.getOffset((Variable)this));
			}
		} else {
			throw new Exception("error allocateAddress");
		}
	}

	public boolean isRecord() {
		if(this instanceof Constant || this instanceof Temp) return false;
		return ((Variable)this).isRecord();
	}
	
	public boolean isArray() {
		if(this instanceof Constant || this instanceof Temp) return false;
		return ((Variable)this).isArray();
	}
	
	public int compareTo(Name an) {
		return this.hashCode() - an.hashCode();
	}
	
	public boolean canAllocateRegister() {
		return this instanceof Temp;// || (this instanceof Variable && !((Variable)this).isGlobal());
	}
}
