package IR;

public class SetReturnValue extends Instruction {
	Name name;
	int u;
	public SetReturnValue(Name name,int u) {
		this.name = name;
		this.u = u;
	}
	
	public String toString() {
		return String.format("Set return value: %s (%d)",name.toString(),u);
	}
	
	public Variable getX() {
		return name instanceof Variable?(Variable)name:null;
	}
	
	public void ReName() {
		name = getX().stack.peek();
	}
	
	public Name getReturnValue() {
		return name;
	}

	@Override
	public Name x() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name y() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name z() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int u() {
		return u;
	}
}
