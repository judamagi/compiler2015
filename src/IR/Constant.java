package IR;

public abstract class Constant extends Name {
	public abstract Object getValue();
}
