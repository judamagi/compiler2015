package IR;

public class Assign extends Instruction {

	/*
	 * x = y, u
	 */
	Variable x;
	Name y;
	int  u;
	public Assign(Variable t1, Name t2, int u) {
		x=t1;
		y=t2;
		this.u = u;
	}

	public String toString() {
		return x.toString()+" = "+y.toString()+String.format("(%d)", u);
	}


	public Name y() {
		return y;
	}

	@Override
	public Name x() {
		return x;
	}

	@Override
	public Name z() {
		return null;
	}

	@Override
	public int u() {
		return u;
	}

}
