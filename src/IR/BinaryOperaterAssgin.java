package IR;

public class BinaryOperaterAssgin extends Instruction {

	/*
	 * x = y op z ,u
	 */
	Variable x;
	Name y,z;
	String op;
	int u;
	public BinaryOperaterAssgin(Variable x, Name y, Name z, String operator) {
		this.x = x;
		this.y = y;
		this.z = z;
		op = operator;
		this.u = 4;
	}
	
	public String toString() {
		String st;
		if(u==1)st=" char";else st=" int";	
		return x.toString()+" = "+y.toString()+" "+op+" "+z.toString();
	}


	public Name y() {
		return y;
	}

	public Name z() {
		return z;
	}
	
	public String getOperatorInMIPS() {
		if(op.equals("+")) return "addu";
		if(op.equals("-")) return "subu";
		if(op.equals("*")) return "mul";
		if(op.equals("/")) return "divu";
		if(op.equals("<<")) return "sllv";
		if(op.equals(">>")) return "srav";
		if(op.equals(">")) return "sgt";
		if(op.equals(">=")) return "sge";
		if(op.equals("<")) return "slt";
		if(op.equals("<=")) return "sle";
		if(op.equals("==")) return "seq";
		if(op.equals("!=")) return "sne";
		if(op.equals("%")) return "rem";
		if(op.equals("&")) return "and";
		if(op.equals("|")) return "or";
		if(op.equals("^")) return "xor";
		return "error";		
	}

	@Override
	public Name x() {
		return x;
	}

	@Override
	public int u() {
		return u;
	}
	
	public String getOperator() {
		return op;
	}

	public void swapYZ() {
		Name k=this.y;
		this.y=this.z;
		this.z=k;
	}
}
