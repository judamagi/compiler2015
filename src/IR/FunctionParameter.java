package IR;

public class FunctionParameter extends Instruction {

	/*
	 * param x (u)
	 */
	Name param;
	int u;
	public FunctionParameter(Name param, int u) {
		this.param = param;
		this.u = u;
	}

	public String toString() {
		return "Param "+param.toString()+String.format("(%d)", u);
	}
	
	public Name param() {
		return param;
	}

	@Override
	public Name x() {
		return param;
	}

	@Override
	public Name y() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name z() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int u() {
		return u;
	}
}
