package IR;

import parser.Type;

public class VariableReName extends Name {

	int subscript;
	Variable ancestor;
	public VariableReName(Variable ancestor) {
		this.ancestor = ancestor;
		subscript = ancestor.createNewName(this);
	}
	
	public String toString() {
//		return name.toString()+"<"+String.valueOf(subscript)+">";
		return null;
	}

	public void popName() {
		ancestor.popName();
	}
	
	public VariableReName TopName() {
		return ancestor.TopName();
	}
	
	public Variable ancestor() {
		return ancestor;
	}
}
