package IR;

public class FunctionSignal extends Instruction {	
	/*
	 * Function Signal (function name) (parameters amount)
	 */
	int amount;
	String name;
	public FunctionSignal(String identifier, int size) {
		name = identifier;
		if(!(name.equals("main") || name.equals("printf")))this.name += "__";
		amount = size;
	}
	
	public String toString() {
		return String.format("Function Signal: %s %d",name,amount);
	}
	
	public String getFunctionName() {
		return name;
	}
	
	public int getAmount() {
		return amount;
	}

	@Override
	public Name x() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name y() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name z() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int u() {
		// TODO Auto-generated method stub
		return 0;
	}
}
