package IR;

public class Label extends Instruction implements Comparable<Label>{
	static int count = 0;
	int label;
	public Label() {
		label = ++count;
	}
	@Override
	public String toString() {
		return "Label"+String.valueOf(label);
	}
	
	public int getID() {
		return label;
	}
	
	@Override
	public int compareTo(Label o) {
		return label-o.getID();
	}
	@Override
	public Name x() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Name y() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Name z() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int u() {
		// TODO Auto-generated method stub
		return 0;
	}
}
