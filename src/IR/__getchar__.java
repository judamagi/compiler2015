package IR;

import java.io.BufferedWriter;
import java.io.IOException;

public class __getchar__ extends Instruction {

	/*
	 *  x = getchar()
	 */
	Temp x;
	int u;
	public __getchar__(Temp temp) {
		x = temp;
		u = 1;
	}

	public String toString() {
		return "__getchar__("+x.toString()+")";
	}
	
	public Temp getChar() {
		return x;
	}

	@Override
	public Name x() {
		return x;
	}

	@Override
	public Name y() {
		return null;
	}

	@Override
	public Name z() {
		return null;
	}

	@Override
	public int u() {
		return 1;
	}
}
