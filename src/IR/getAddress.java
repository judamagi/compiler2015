package IR;

public class getAddress extends Instruction {

	/*
	 * x = &y
	 */
	Name x,y;
	Name z = null;
	int u=4;
	public getAddress(Name x,Name y) {
		this.x = x;
		this.y = y;
	}
	
	public getAddress(Name x, Name y, Name z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public String toString() {
		String temp  = x.toString()+" = &"+y.toString();
		if(z != null) temp = String.format("%s[%s]", temp,z.toString());
		return temp;
	}
	
	public Variable getX() {
		return (Variable) x;
	}

	public Variable getY() {
		return y instanceof Variable?(Variable)y:null;
	}
	
	public Name x() {
		return x;
	}
	
	public Name y() {
		return y;		
	}
	
	public Name z() {
		return z;
	}

	@Override
	public int u() {
		return u;
	}
}
