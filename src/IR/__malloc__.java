package IR;

import java.io.BufferedWriter;
import java.io.IOException;
import Env.*;

public class __malloc__ extends Instruction {
	Name y;
	Temp x;
	int u;
	/*
	 *  x=malloc(y)
	 */
	public __malloc__(Temp x,Name length) {
		this.y = length;
		this.x = x;
		u = 4;
	}
	
	public Temp getReturnName() {
		return x;
	}
	
	public String toString() {
		return x.toString()+" = malloc("+y.toString()+")";
	}

	public Name getLength() {
		return y;
	}

	@Override
	public Name x() {
		return x;
	}

	@Override
	public Name y() {
		return y;
	}

	@Override
	public Name z() {
		return null;
	}

	@Override
	public int u() {
		return u;
	}
}
