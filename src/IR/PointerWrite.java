package IR;

public class PointerWrite extends Instruction {
	/*
	 *  *x = y, u
	 */
	Name x,y;
	int u;
	public PointerWrite(Name x , Name y,int u) {
		this.x = x;
		this.y = y;
		this.u = u;
	}
	
	public String toString() {
		return "*"+x.toString()+" = "+y.toString()+String.format("(%d)", u);
	}
	
	public Variable getX() {
		return (Variable) x;
	}

	public Variable getY() {
		return y instanceof Variable?(Variable)y:null;
	}
	

	public Name y() {
		return y;
	}

	@Override
	public Name x() {
		return x;
	}

	@Override
	public Name z() {
		return null;
	}

	@Override
	public int u() {
		return u;
	}
}
