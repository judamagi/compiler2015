package IR;

import error.SemanticError;
import parser.Array;
import parser.Parser;
import parser.Pointer;
import parser.StringLiteral;
import parser.Type;
import Env.StaticData;

public class __String__ extends Constant {

	static int count = 0;
	
	String str;

	private String address;
	public __String__(String stringLiteral) {
		str = stringLiteral;
		address = StaticData.addStringConstant(str);
		//String msg = String.format("STR__%d", count++);
		
	}
	
	public String toString() {
		return address;
	}

	@Override
	public Object getValue() {
		return address;
	}

}
