package IR;

import MIPS.MIPSWithSimpleGeneration;
import parser.Type;

public class Temp extends Variable {
	static int count = 0;
	int label;
	public Temp() {
		super("Temp"+String.valueOf(count+1),false,false);
		label = ++ count;
	}
	public String toString() {
		if(MIPSWithSimpleGeneration.assignTable.containsKey(this)) 
			return String.format("%s(%s)", name,MIPSWithSimpleGeneration.assignTable.get(this));
		return name;
	}
}
