package IR;

public class FunctionCall extends Instruction {

	/*
	 *  call function res
	 */
	String functionName;
	Variable res;
	int returnSize; // u
	public FunctionCall(String identifier, Temp temp, int size_t) {
		functionName = identifier;
		if(!(functionName.equals("main") || functionName.equals("printf")))this.functionName += "__";
		res = temp;
		returnSize = size_t;
	}

	public String toString() {
		return String.format("Call: %s(size: %d) <-- %s", res.toString(),returnSize,functionName);
	}
	
	public String getCallee() {
		return functionName;
	}
	
	public Variable getReturnValue() {
		return res;
	}
	
	public int getReturnSize() {
		return returnSize;
	}

	@Override
	public Name x() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name y() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name z() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int u() {
		return returnSize;
	}
}
