package IR;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import SSA.ControlFlowGraph;
import SSA.StaticSingleAssignmentForm;

public abstract class Instruction {
	static LinkedList<Instruction> instructions = new LinkedList<>();
	static public void add(Instruction e) {
		instructions.add(e);
		//System.err.println(e.toString());
	}
	static public Instruction getLastInstruction() {
		return instructions.getLast();
	}
	static public void removeLastInstruction() {
		instructions.removeLast();
	}
	static public void print() {
		int i = 0;
		for (Instruction instruction : instructions) {
			System.err.printf("<%d> :  ",++i);
			System.err.println(instruction.toString());
			if(instruction  instanceof FunctionReturn) System.err.println();
		}
	}
	static public List<ControlFlowGraph> createCFG() {
		List<ControlFlowGraph> res = new ArrayList<ControlFlowGraph>();
		ControlFlowGraph temp = null;
		for (Instruction iter : instructions) {
			if(iter instanceof FuntionStart){
				temp = new ControlFlowGraph((FuntionStart)iter);
			} else {
				temp.add(iter);
				if(iter instanceof FunctionReturn){
					res.add(temp);
				}
			}
		}
		return res;
	}
	static public List<Instruction> MIPSGeneration() {
		return instructions;
	}
	
	public abstract Name x();
	public abstract Name y();
	public abstract Name z();
	public abstract int u();
}
