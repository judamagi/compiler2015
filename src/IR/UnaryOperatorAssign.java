package IR;

public class UnaryOperatorAssign extends Instruction {
	/*
	 * x = Op y
	 */
	String Op;
	Variable x;
	Name y;
	int u;
	public UnaryOperatorAssign(Variable x,String op, Name y) {
		this.Op = op;
		this.x = x;
		this.y = y;
		this.u = 4;
	}
	
	public String toString() {
		String st;
		if(u==1)st=" char";else st=" int";
		return x.toString()+" = "+Op+y.toString();
	}

	
	public String getOperator() {
		return Op;
	}

	public Name y() {
		return y;
	}

	@Override
	public Name x() {
		return x;
	}

	@Override
	public Name z() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int u() {
		return u;
	}
}
