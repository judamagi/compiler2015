package IR;

import parser.Parser;
import parser.Pointer;
import parser.Type;

public class CharConstant extends Constant {

	char ch;
	public CharConstant(char characterLiteral) {
		ch = characterLiteral;
	}
	
	public String toString() {
		return "'"+String.valueOf(ch)+"'";
	}

	@Override
	public Object getValue() {
		return (int)ch;
	}

	@Override
	public boolean equals(Object an) {
		if(an!=null && an instanceof Character) {
			return (char)((CharConstant) an).getValue() == ch;
		}
		return false;
	}
}
