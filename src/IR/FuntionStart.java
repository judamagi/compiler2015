package IR;

public class FuntionStart extends Instruction {
	String name;
	int args;
	public FuntionStart(String name,int args) {
		this.name = name;
		if(!(name.equals("main") || name.equals("printf")))this.name += "__";
		this.args = args;
	}
	
	public String toString() {
		return String.format("Function %s (%d args)", name,args);
	}
	
	public String getName() {
		return name;
	}

	@Override
	public Name x() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name y() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name z() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int u() {
		// TODO Auto-generated method stub
		return 0;
	}
}
