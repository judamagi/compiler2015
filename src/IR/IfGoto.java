package IR;

public class IfGoto extends Instruction {
	Name name;
	Label label;
	public IfGoto(Name name, Label label) {
		this.name = name;
		this.label = label;
	}
	
	public String toString() {
		return "IF "+name.toString()+" goto "+label.toString();
	}
	
	public Label getLabel() {
		return label;
	}
	
	public Variable getX() {
		return name instanceof Variable?(Variable)name:null;
	}
	
	public void ReName() {
		name = getX().stack.peek();
	}

	@Override
	public Name x() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name y() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name z() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int u() {
		// TODO Auto-generated method stub
		return 0;
	}
}
