package IR;

public class LocalVariableRequest extends Instruction {
	Variable name;
	int size;
	public LocalVariableRequest(Variable name, int size) {
		this.name = name;
		this.size = size;
	}
	
	public String toString() {
		return "Local variable request:(length "+String.valueOf(size)+" ) "+name.toString();
	}
	
	public Variable getVariable() {
		return name;
	}
	
	public int getSize() {
		return size;
	}


	@Override
	public Name x() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name y() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name z() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int u() {
		// TODO Auto-generated method stub
		return 0;
	}
}
