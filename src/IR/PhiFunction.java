package IR;

import java.util.ArrayList;
import java.util.List;

public class PhiFunction extends Instruction {
	/*
	 * x = phi(...)
	 */
	Variable x;
	List<VariableReName> parameters = new ArrayList<>();
	
	public PhiFunction(Variable x) {
		this.x = x;
	}
	
	public Variable getVariable() {
		return x;
	}
	
	public String toString() {
		String res = x.toString()+" = phi(";
		/*for (Variable x : parameters) {
			res+=x+",";
		}*/
		return res+")";
	}
	
	public void reNameX() {
		//this.x = new VariableReName(x);
	}
	
	public void addParameter(VariableReName x) {
		parameters.add(x);
	}

	@Override
	public Name x() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name y() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Name z() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int u() {
		// TODO Auto-generated method stub
		return 0;
	}
}
