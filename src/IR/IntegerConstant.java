package IR;

import parser.Parser;
import parser.Type;

public class IntegerConstant extends Constant {

	int x;
	public IntegerConstant(int s) {
		this.x = s;
	}

	public String toString() {
		return String.valueOf(x);
	}
	
	public Object getValue() {
		return x;
	}

	@Override
	public boolean equals(Object an) {
		if(an!=null && an instanceof IntegerConstant ) {
			return ((IntegerConstant)an).x == x;
		}
		return false;
	}
}
