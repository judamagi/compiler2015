package IR;

import java.util.HashMap;
import java.util.Map;

public class StringConstant{
	static Map<String, Integer> constant = new HashMap<String, Integer>();

	static public void addNewString(String x) {
		if(!constant.containsKey(x))constant.put(x, gen());
	}
	
	static public int getStringAddress(String x) {
		return constant.get(x);
	}
	
	static private Integer gen() {
		// 
		return 10000000+(int)(Math.random()*1000000);
	}

}
