package IR;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import parser.Type;
import SSA.Block;

public class Variable extends Name {

	String name;
	int counter = 0;
	Stack<VariableReName> stack;
	public Set<Block> blocks = new HashSet<>();
	private boolean isRecord,isArray;
	
	public void reNameInitilization() {
		stack = new Stack<VariableReName>();
	}
	
	public Variable(String valName, boolean isRecord,boolean isArray) {
		name = valName;
		this.isRecord = isRecord;
		this.isArray = isArray;
	}	

	public String toString() {
		return name;
	}
	
	public void addBlocks(Block x) {
		blocks.add(x);
	}

	final public boolean isGlobal() {
		return !(this instanceof Temp) && !name.contains("#");
	}

	public int createNewName(VariableReName variableReName) {
		stack.push(variableReName);
		return counter++;
	}
	
	public VariableReName TopName() {
		return stack.peek();
	}
	
	public void popName() {
		stack.pop();
	}

	public String globalAddress() {
		if(name.equals("j")) return "jjj";
		if(name.equals("b")) return "bbb";
		return name;
	}

	public boolean isRecordOrArray() {
		return isRecord || isArray;
	}
	
	public boolean isRecord() {
		return isRecord;
	}
	
	public boolean isArray() {
		return isArray;
	}
	
	public boolean isParam() {
		return name.endsWith("1");
	}
}
