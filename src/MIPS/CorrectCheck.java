package MIPS;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import error.IRError;
import lexer.Lexer;
import parser.Parser;
import parser.TranslationUnitDecl;
import IR.Instruction;
import SSA.ControlFlowGraph;

public class CorrectCheck {
	private static Parser parser;
	private static BufferedWriter output;

	public static void main(String[] args) throws IOException {
		if(args.length == 0) {
			parser = new Parser(new Lexer());
			output = new BufferedWriter(new OutputStreamWriter(System.out));
		} else {
			parser = new Parser(new Lexer(args[0]));
			output = new BufferedWriter(new FileWriter("mips.s"));
		}		
		TranslationUnitDecl res;
		try {
			res = parser.parseProgram();			
			res.check(null);
			List<Instruction> ISA = optimist();
			//showIRList(ISA);
			MIPSWithSimpleGeneration.work(output,ISA);
			output.flush();
		} catch (Exception e) {			
			e.printStackTrace();
		} finally {
			output.close();
		}		
	}

	private static void showIRList(List<Instruction> iSA) {
		int c = 0;
		for(Instruction iter:iSA) {			
			System.err.println(String.format("<%3d> %s", ++c,iter.toString()));
		}		
	}

	private static List<Instruction> optimist() throws IRError {
		List<Instruction> res = new ArrayList<Instruction>();
		List<ControlFlowGraph> CFGs = Instruction.createCFG();	
		for (ControlFlowGraph iter : CFGs) {
			iter.transferIRToCFG();
			iter.calcConstant();
			iter.eliminateIdenticalEqution();
			iter.extendLVN();
			iter.swapNumber();
			iter.appendIR(res);
		}
		return res;
	}
}
