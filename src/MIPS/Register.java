package MIPS;

import IR.Temp;
import IR.Variable;

public class Register {
	Variable conserve;
	int serial;
	public Register(int serial) {
		this.serial = serial;
		this.conserve = null;
	}
	public String toString() {
		return String.format("$%d", serial);
	}
	public void setConserve(Variable x) {
		conserve = x;
	}
	public Variable getConserve() {
		return conserve;
	}
}
