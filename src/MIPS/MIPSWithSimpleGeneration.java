package MIPS;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import parser.Array;
import parser.CHAR;
import parser.Record;
import error.SemanticError;
import Env.IREnv;
import Env.StaticData;
import IR.Assign;
import IR.BinaryOperaterAssgin;
import IR.Constant;
import IR.FunctionCall;
import IR.FunctionParameter;
import IR.FunctionReturn;
import IR.FunctionSignal;
import IR.FuntionStart;
import IR.Goto;
import IR.IfFalseGoto;
import IR.IfGoto;
import IR.Instruction;
import IR.IntegerConstant;
import IR.Label;
import IR.LocalVariableRequest;
import IR.Name;
import IR.PointerWrite;
import IR.SetReturnValue;
import IR.StringConstant;
import IR.Temp;
import IR.UnaryOperatorAssign;
import IR.Variable;
import IR.__Printf__;
import IR.__String__;
import IR.__getchar__;
import IR.__malloc__;
import IR.getAddress;
import IR.solvePointer;

final public class MIPSWithSimpleGeneration {
	static BufferedWriter output;
	static Map<String, IREnv> FunctionLists = new TreeMap<>();
	
	static void collectInfomation(List<Instruction> iNSTRUCTION) throws SemanticError {
		IREnv nowEnv = null;
		Name x;
		for (Instruction iter:iNSTRUCTION) {
			int u;
			if(iter instanceof FuntionStart) {
				nowEnv = new IREnv(((FuntionStart) iter).getName());
				FunctionLists.put(((FuntionStart) iter).getName(),nowEnv);
			}
			if(iter instanceof LocalVariableRequest) {
				nowEnv.add(((LocalVariableRequest) iter).getVariable(),((LocalVariableRequest) iter).getSize());
			}
			if(iter instanceof Assign) {
				x = ((Assign) iter).x();
				if(x instanceof Temp && nowEnv.notContain(x)) nowEnv.add((Variable) x,iter.u());
			}
			if(iter instanceof BinaryOperaterAssgin){
				x = ((BinaryOperaterAssgin) iter).x();
				if(x instanceof Temp && nowEnv.notContain(x)) nowEnv.add((Variable) x,4);
			}
			if(iter instanceof UnaryOperatorAssign) {
				x = ((UnaryOperatorAssign) iter).x();
				if(x instanceof Temp && nowEnv.notContain(x)) nowEnv.add((Variable) x,4);
			}
			if(iter instanceof getAddress) {
				x = ((getAddress) iter).x();
				if(x instanceof Temp && nowEnv.notContain(x)) nowEnv.add((Variable) x,4);
			}
			if(iter instanceof solvePointer) {
				x = ((solvePointer) iter).x();
				if(x instanceof Temp && nowEnv.notContain(x)) nowEnv.add((Variable) x,iter.u());
			}			
			if(iter instanceof FunctionCall) {
				x = ((FunctionCall) iter).getReturnValue();
				nowEnv.add((Variable) x,((FunctionCall) iter).getReturnSize());
			}
			if(iter instanceof FunctionReturn) {				
				nowEnv = null;
			}
			if(iter instanceof __malloc__) {
				nowEnv.add(((__malloc__) iter).getReturnName(), 4);
			}
			if(iter instanceof __getchar__) {
				nowEnv.add(((__getchar__) iter).getChar(),4);
			}
		}
	}
	
	static public String getRegister(Name x,String register, IREnv nowEnv) throws IOException, Exception {
		if(x instanceof Temp && assignTable.containsKey(x)) {
			Register cc = assignTable.get(x);
			if(cc.getConserve()!=null && cc.getConserve()!=x){
				gen(cc.getConserve().storeValue(cc.toString(), nowEnv, 'w', 0));				
			}
			cc.setConserve((Temp) x);
			register = cc.toString();
		} return register;
	}
	
	static void generateFunctionsCode(List<Instruction> iNSTRUCTION) throws Exception {
		IREnv nowEnv = null;
		int frameSize = 0, param = 0 ;
		int ii=0;
		for(Instruction iter:iNSTRUCTION) {
			++ii;
			//System.err.println(++ii);
			gen("\t #"+iter.toString());
			if(iter instanceof FuntionStart) {
				nowEnv = FunctionLists.get(((FuntionStart) iter).getName());				
				gen();
				gen(((FuntionStart) iter).getName()+":");
				if(((FuntionStart) iter).getName().equals("main")) {
					gen(String.format("\t addu $sp, $sp, -%d",nowEnv.getSize()));
					gen("\t li $v1, 37"); // %
					gen("\t li $gp, 100"); // 'd'
				}
				gen("\t sw $ra, 0($sp)");
			}
			if(iter instanceof FunctionReturn) {
				for(Register it:registerLists)it.setConserve(null);
				gen("\t lw $ra, ($sp)");				
				gen("\t addu $sp, $sp, "+String.valueOf(nowEnv.getSize()));
				if(((FunctionReturn) iter).isMain()) {
					gen("\t li $v0 10");
					gen("\t syscall");
				} else {
					gen("\t jr $ra");
				}
			}
			if(iter instanceof FunctionCall) {
				for(Register it:registerLists) {
					if(it.getConserve() != null) {
						gen(it.getConserve().storeValue(it.toString(), nowEnv, 'w', 0));
						it.setConserve(null);
					}
				}
				gen("\t move $sp, $fp");
				gen(String.format("\t jal %s", ((FunctionCall) iter).getCallee()));
				Name x = ((FunctionCall) iter).getReturnValue();
				int u = ((FunctionCall) iter).getReturnSize();
				if(u > 4) {					
					for(int i=0;i<u;i+=4) {
						gen(String.format("\t lw $a0, %d($v0)", i));
						gen(x.storeValue("$a0", nowEnv, 'w', i));
					}
				} else {
					gen(x.storeValue("$v0", nowEnv, 'w', 0));
				}
				param = 0;
			}
			if(iter instanceof FunctionSignal) {
				String res = ((FunctionSignal) iter).getFunctionName();
				if(res.equals("printf")) {
					frameSize = ((FunctionSignal) iter).getAmount() * 4+4;
				} else {
					frameSize = FunctionLists.get(res).getSize();
				}
				gen(String.format("\t subu $fp, $sp, %d",frameSize));
			}
			if(iter instanceof FunctionParameter) {				
				Name x = iter.x();
				int u = iter.u() ;
				char CH =  u==1?'b':'w';
				String cd;
				if(x.isArray()) {
					u=4;
					cd = x.allocateValue("$a0", nowEnv, 'w',0);
					gen(String.format("\t sw %s, %d($fp)", cd,4+param));
				} else {
					for(int i=0;i<u;i+=4) {
						if(x.isRecord()) {
							gen(x.allocateAddress("$a1", nowEnv, 0));
							gen(String.format("\t lw $a0, %d($a1)", i));
							cd = "$a0";
						} else {
							cd = x.allocateValue("$a0", nowEnv, CH, i);						
						}
						gen(String.format("\t s%c %s , %d($fp)", CH,cd,4+i+param));
					}
				}
				param += Math.max(4, u);
			}			
			if(iter instanceof getAddress) {
				Name x = iter.x(),y = iter.y();
				String cd ="$a0";
				//String cd = getRegister(x, "$a0", nowEnv);				
				gen(y.allocateAddress(cd, nowEnv, 0));
				//if(cd.equals("$a0"))
				gen(x.storeValue(cd, nowEnv, 'w', 0));
			}
			if(iter instanceof solvePointer) {
				Name x = iter.x(), y = iter.y();
				int u = iter.u();
				char CH = u==1?'b':'w';
				String cd,cc = getRegister(x, "$a1", nowEnv);
				cd = y.allocateValue("$a0", nowEnv, 'w', 0);
				for(int i=0;i<u;i+=4) {					
					gen(String.format("\t l%c %s, %d(%s)",CH,cc,i,cd));
					if(cc.equals("$a1"))
						gen(x.storeValue("$a1", nowEnv, CH, i));
				}
			}
			if(iter instanceof PointerWrite) {
				Name x = iter.x(), y = iter.y();
				int u = iter.u();
				char CH = u==1?'b':'w';
				String cd = x.allocateValue("$a1", nowEnv, 'w', 0), cc;
				for(int i=0;i<u;i+=4) {
					cc = y.allocateValue("$a0", nowEnv, CH, i);
					gen(String.format("\t s%c %s, %d(%s)", CH,cc,i,cd));
				}
			}
			if(iter instanceof Assign) {
				Name x = iter.x(), y = iter.y();
				int u = iter.u();
				if(y instanceof __String__ && x.isArray()) {
					for(int i=0;i<u;i++){
						gen(String.format("\t lb $a0, %s+%d", ((__String__) y).getValue(),i));
						gen(x.storeValue("$a0", nowEnv, 'b', i));
					}
				} else {					
					char CH = u==1?'b':'w';
					String cd;
					for(int i=0;i<u;i+=4) {					
						cd = y.allocateValue("$a0", nowEnv, CH, i);
						gen(x.storeValue(cd, nowEnv, CH, i));
					}
				}
			}
			if(iter instanceof BinaryOperaterAssgin) {
				Name x = iter.x(), y = iter.y(), z = iter.z();
				String cd1,cd2,cd3;
				cd1 = y.allocateValue("$a0", nowEnv, 'w', 0);
				if(z instanceof IntegerConstant) {
					cd2 = z.toString();
				} else {
					cd2 = z.allocateValue("$a1", nowEnv, 'w', 0);
				}
				cd3 = getRegister(x, "$a2", nowEnv);
				gen(String.format("\t %s %s, %s, %s", ((BinaryOperaterAssgin) iter).getOperatorInMIPS(),cd3,cd1,cd2));
				if(cd3.equals("$a2"))
					gen(x.storeValue(cd3, nowEnv, 'w', 0));
							
			}
			if(iter instanceof Goto){
				gen("\t b "+((Goto)iter).getLabel().toString());
			}
			if(iter instanceof IfFalseGoto) {
				Name x = ((IfFalseGoto) iter).getX();
				String cd = x.allocateValue("$a0", nowEnv, 'w', 0);
				gen(String.format("\t beqz %s, %s", cd,((IfFalseGoto) iter).getLabel().toString()));
			}
			if(iter instanceof IfGoto) {
				Name x = ((IfGoto) iter).getX();
				String cd = x.allocateValue("$t0", nowEnv, 'w', 0);
				gen(String.format("\t bnez %s, %s", cd,((IfGoto) iter).getLabel().toString()));
			}
			if(iter instanceof Label) {
				gen(iter.toString()+":");
			}
			if(iter instanceof SetReturnValue) {
				Name x = ((SetReturnValue) iter).getReturnValue();
				int u = iter.u();
				if(u>4) {
					gen(x.allocateAddress("$v0", nowEnv, 0));
				} else {
					String cd = x.allocateValue("$v0", nowEnv, 'w', 0);
					if(!cd.equals("$v0")) gen("\t move $v0, "+cd);
				}
			}
			if(iter instanceof UnaryOperatorAssign) {
				Name x = iter.x(), y = iter.y();
				String cd = y.allocateValue("$a0", nowEnv, 'w', 0);
				String cd1 = getRegister(x, "$a2", nowEnv);
				String op = ((UnaryOperatorAssign) iter).getOperator();
				if(op.equals("+")) {
					gen(String.format("\t move %s, %s",cd1,cd));
				}
				if(op.equals("-")) {
					gen(String.format("\t negu %s, %s",cd1,cd));
				}
				if(op.equals("~")) {
					gen(String.format("\t not %s, %s",cd1,cd));
				}
				if(op.equals("!")) {
					gen(String.format("\t seq %s, %s, $zero",cd1,cd));
				}
				if(cd1.equals("$a2"))
					gen(x.storeValue(cd1, nowEnv, 'w', 0));
			}
			if(iter instanceof __getchar__) {
				gen("\t li $v0 12");
				gen("\t syscall");
				Name x = ((__getchar__) iter).getChar();
				gen(x.storeValue("$v0", nowEnv, 'w', 0));
			}
			if(iter instanceof __malloc__) {
				Temp x = ((__malloc__) iter).getReturnName();
				Name y = ((__malloc__) iter).getLength();
				String cd = y.allocateValue("$a0", nowEnv, 'w', 0);
				if(!cd.equals("$a0")) gen(String.format("\t move $a0, %s", cd));
				gen("\t li $v0, 9");
				gen("\t syscall");
				gen(x.storeValue("$v0", nowEnv, 'w', 0));
			}
			if(iter instanceof __Printf__) {
				for(Register it:registerLists) {
					if(it.getConserve() != null) {
						gen(it.getConserve().storeValue(it.toString(), nowEnv, 'w', 0));
						it.setConserve(null);
					}
				}
				gen("\t move $sp, $fp");
				gen(String.format("\t la $a1, %s", ((__Printf__) iter).getFormatID()));
				gen("\t jal printf_loop");
				gen(String.format("\t addu $sp, $sp, %d", frameSize));
				param = 0;
			}
		}
	}
	
	static void work(BufferedWriter output, List<Instruction> INSTRUCTION) throws Exception {
		MIPSWithSimpleGeneration.output = output;
		collectInfomation(INSTRUCTION);
		allocateRegister(INSTRUCTION);
		// .text
		output.append(".text\n");
		output.append(".globl main\n");
		//output.append("\t j main\n");
		generateFunctionsCode(INSTRUCTION);
		generatePrintfCode(output);
		// .data
		output.append("\n");
		output.append(".data\n");
		StaticData.expression();			
	}
	
	
	static public Map<Variable, Register> assignTable = new HashMap<>();
	static ArrayList<Register> registerLists = new ArrayList<>();
	static List<segment> waitTable = new ArrayList<>();
	private static void allocateRegister(List<Instruction> iNSTRUCTION) {
		for(int i=8;i<=25;i++) registerLists.add(new Register(i));
		for(Instruction iter:iNSTRUCTION) {
			if(iter instanceof BinaryOperaterAssgin) {
				Name x = iter.x(), y = iter.y(), z = iter.z();
				if(x.canAllocateRegister() && waitTable.indexOf(x)==-1) waitTable.add(new segment((Variable) x, 0));
				if(y.canAllocateRegister() && waitTable.indexOf(y)==-1) waitTable.add(new segment((Variable) y, 0));
				if(z.canAllocateRegister() && waitTable.indexOf(z)==-1) waitTable.add(new segment((Variable) z, 0));
			}
		}
		int ii=0 ,zjbl;
		zjbl = waitTable.size();
		for(Instruction iter:iNSTRUCTION) {
			++ii;
			if(iter instanceof LocalVariableRequest) continue;
			Name x = iter.x(), y = iter.y(), z = iter.z();
			if(waitTable.indexOf(x)!=-1) {
				int k = waitTable.indexOf(x);
				segment nana = waitTable.get(k);
				if(nana.x==0){
					waitTable.set(k, new segment(nana.interval, ii*10000+ii));
				} else {
					int st = nana.x/10000;
					waitTable.set(k, new segment(nana.interval, st*10000+ii));
				}
			}
			if(waitTable.indexOf(y)!=-1) {
				int k = waitTable.indexOf(y);
				segment nana = waitTable.get(k);
				if(nana.x==0){
					waitTable.set(k, new segment(nana.interval, ii*10000+ii));
				} else {
					int st = nana.x/10000;
					waitTable.set(k, new segment(nana.interval, st*10000+ii));
				}
			}
			if(waitTable.indexOf(z)!=-1) {
				int k = waitTable.indexOf(z);
				segment nana = waitTable.get(k);
				if(nana.x==0){
					waitTable.set(k, new segment(nana.interval, ii*10000+ii));
				} else {
					int st = nana.x/10000;
					waitTable.set(k, new segment(nana.interval, st*10000+ii));
				}
			}
		}
		Collections.sort(waitTable, new Comparator<segment>() {

			@Override
			public int compare(segment o1, segment o2) {
				//int t1 = o1.x/10000*20000 - o2.x%10000;
				//int t2 = o2.x/10000*20000 - o2.x%10000;
				int t1 = o1.x, t2 = o2.x;
				return t1-t2;
			}
			
		});
		for(segment it:waitTable) {
			int k = it.x/10000;
			for(Register re:registerLists) {
				if(re.conserve == null) {
					re.conserve = it.interval;
					assignTable.put(it.interval, re);
					break;
				} else {
					Variable zj = re.conserve;
					int kk;
					for(kk=0;kk<waitTable.size();kk++){
						if(waitTable.get(kk).interval == zj)break;
					}
					int ed = waitTable.get(kk).x%10000;
					if(ed > k) {
						re.conserve = it.interval;
						assignTable.put(it.interval, re);
						break;
					}
				}
			}
		}
		zjbl = assignTable.size();
		for(Register it:registerLists) it.setConserve(null);
	}

	@SuppressWarnings("unused")
	private static Register bruteForceAssign(Name x) {
		for(Register iter:registerLists){
			if(iter.getConserve() == null) {
				iter.setConserve((Temp) x);
				return iter;
			}
		}
		return null;
	}

	static public void gen(String x) throws IOException {
		//System.err.println(x);
		MIPSWithSimpleGeneration.output.append(x+"\n");
	}
	static public void gen() throws IOException {
		MIPSWithSimpleGeneration.output.append("\n");
	}
	static void generatePrintfCode(BufferedWriter output) throws IOException {
		gen("printf_loop:");
		gen("\t addiu $t1, $sp, 4");		
		gen("\t li $t5, 99"); // 'c'
//		gen("\t li $t6, 115"); // 's'
		
		gen("printf_x:");
		gen("\t lb $t0, ($a1)");
		gen("\t beq $t0, 0, printf_end");
		gen("\t addiu $a1, $a1, 1");
		gen("\t beq $t0, $v1, printf_stmt"); // %
		gen("\t li $v0, 11");
		gen("\t lb $a0, -1($a1)");
		gen("\t syscall");
		gen("\t b printf_x");		
		
		gen("printf_stmt:");
		gen("\t lb $t2 ($a1)");
		gen("\t addiu $a1, $a1, 1");
		gen("\t addiu $t1, $t1, 4");
		gen("\t beq $t2, $gp, printf_int"); // 'd'
		gen("\t beq $t2, $t5, printf_char"); // 'c'
		gen("\t beq $t2, 115, printf_str"); // 's'
		gen("\t beq $t2, '0', printf_0"); // '0'
		gen("\t beq $t2, '.', printf_0"); // '.'
		
		gen("printf_int:");
		gen("\t lw $a0, -4($t1)");
		gen("\t li $v0, 1");
		gen("\t syscall");
		gen("\t b printf_x");
		gen("printf_char:");
		gen("\t lb $a0, -4($t1)");
		gen("\t li $v0, 11");
		gen("\t syscall");
		gen("\t b printf_x");
		gen("printf_str:");
		gen("\t lw $a0, -4($t1)");
		gen("\t li $v0, 4");
		gen("\t syscall");
		gen("\t b printf_x");
		gen("printf_0:");
		gen("\t lb $t2, ($a1)");
		gen("\t addiu $a1, $a1, 1");
		gen("\t addiu $a1, $a1, 1");
		gen("\t lw $a2, -4($t1)");		
		gen("\t li $a0, 0");
		gen("\t li $v0, 1");
		gen("\t beq $t2, '3', printf_3");
		gen("\t li $a3, 999");
		gen("\t bgt $a2, $a3, printf_int");
		gen("\t syscall");
		gen("printf_3:");
		gen("\t li $a3, 99");
		gen("\t bgt $a2, $a3, printf_int");
		gen("\t syscall");
		gen("\t li $a3, 9");
		gen("\t bgt $a2, $a3, printf_int");
		gen("\t syscall");
		gen("\t b printf_int");
		gen("printf_end:");
		gen("\t jr $ra");
		
		
		gen();
	}
}
