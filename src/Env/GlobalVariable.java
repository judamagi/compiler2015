package Env;

import java.util.ArrayList;
import java.util.List;

import parser.Type;

public class GlobalVariable {

	String VarName;
	int size;
	List<Integer> wordInt = new ArrayList<Integer>();
	List<Character> wordChar = new ArrayList<>();
	String str = null;
	int kind = -1;
	public GlobalVariable(String varName, int size) {
		VarName = varName;
		this.size = size;
	}
	public void update(List<Integer> x) {
		wordInt = x;
		kind = 0;
	}
	public void update(String value) {
		kind = 1;
		str = value;
	}
	public void update(List<Character> x,boolean Char) {
		kind = 2;
		wordChar = x;
	}
	public String expression() {
		String res = "", VarName = new String(this.VarName);
		if(VarName.equals("j")) VarName = "jjj";
		if(VarName.equals("b")) VarName = "bbb";
		if(kind == 1) {
			res = String.format("%s: .asciiz \"%s\"", VarName,str);
		}
		if(kind == 0) {
			res = VarName+": .word";
			for(Integer iter: wordInt) {
				res += " "+String.valueOf(iter);
			}
		}
		if(kind == 2) {
			res = VarName+": .word";
			for(char iter:wordChar) {
				res += " "+String.valueOf((int)iter);
			}
		}
		if(kind == -1) res = String.format("%s: .space %d", VarName,size);
		return res;
	}

}
