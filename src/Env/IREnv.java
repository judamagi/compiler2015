package Env;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import parser.Array;
import IR.Name;
import IR.Variable;

public class IREnv {

	Set<Variable> hash = new HashSet<>();
	List<Variable> vars = new ArrayList<>();
	List<Integer> size_t = new ArrayList<>();
	
	String FunName;
	
	public IREnv(String name) {
		FunName = name+"_";
	}
	public void add(Variable variable, int size) {
		if(!hash.contains(variable)) {
			hash.add(variable);
			vars.add(variable);
			size_t.add(Math.max(4,size));
		}
	}

	public int getSize() {
		int res = 0;
		for(Integer iter:size_t) {
			res +=  iter;
		}
		return res+4;
	}
	public boolean notContain(Name x) {
		return !hash.contains(x);
	}
	public int getVariableSize(Variable x) {
		if(x.isGlobal()){
			return 4;
		}
		return size_t.get(vars.indexOf(x));
	}
	public int getOffset(Variable x) {
		int res = 0;
		for(int i=0;i<vars.size();++i) {
			if(vars.get(i) == x) break;
			res += size_t.get(i);
		}
		return res+4;
	}

}
