package Env;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import IR.StringConstant;
import IR.Variable;
import MIPS.MIPSWithSimpleGeneration;
import parser.Array;
import parser.CHAR;
import parser.Stmt;
import parser.StringLiteral;
import parser.Type;

public class StaticData {
	static int gpUp = 0x10008000, gpDown = gpUp;
	static public int __malloc__(int size_t) {
		size_t = ((size_t-1)/4+1)*4;
		int temp = gpUp;
		gpUp += size_t;
		return temp;
	}

	static Map<String, GlobalVariable> vars = new HashMap<String, GlobalVariable>();
	static Map<String, Integer> varSize = new HashMap<>();
	static Set<String> specialAddress = new HashSet<>();
	public static void addVariable(String varName, Type baseType,int size_t) {
		GlobalVariable tt = new GlobalVariable(varName,size_t);
		vars.put(varName,tt);
		varSize.put(varName, size_t);
	}

	public static void update(String temp, List<Object> x, Type baseType) {
		GlobalVariable tt = vars.get(temp);
		if (baseType instanceof CHAR) {
			List<Integer> y = new ArrayList<Integer>(x.size());
			for(Object iter: x){
				y.add((int) iter);				
			}
			tt.update(y);
		} else {
			List<Integer> y = new ArrayList<Integer>(x.size());
			for(Object iter: x){
				y.add((Integer) iter);
			}
			tt.update(y);
		}
	}
	
	public static boolean isGlobalString(String temp) {
		return specialAddress.contains(temp);
	}

	public static void update(String temp, String value) {
		GlobalVariable tt = vars.get(temp);
		tt.update(value);
		StaticData.specialAddress.add(temp);
	}

	public static void update(String temp, int x) {
		GlobalVariable tt = vars.get(temp);
		ArrayList<Integer> res = new ArrayList<>();
		res.add(x);
		tt.update(res);
	}

	public static void update(String temp, List<Integer> x) {
		GlobalVariable tt = vars.get(temp);
		tt.update(x);
	}
	
	public static void update(String temp, List<Character> x, boolean s) {
		GlobalVariable tt = vars.get(temp);
		tt.update(x,s);
	}
	
	public static void expression() throws IOException {
		String res;
		Iterator<Entry<String, GlobalVariable>> iter = vars.entrySet().iterator();
		while(iter.hasNext()) {			
			res = iter.next().getValue().expression();
			MIPSWithSimpleGeneration.gen(res);
		}
		int i=0;
		for(String it:wordStr) {
			MIPSWithSimpleGeneration.gen(String.format("__STR__%d: .asciiz \"%s\"",i++,it));
		}
	}
	
	static List<String> wordStr = new ArrayList<>();
	static int StrCount = 0;
	public static String addStringConstant(String str) {
		wordStr.add(str);
		return String.format("__STR__%d", StrCount++);
	}

	public static int getVariableSize(Variable x) {
		return varSize.get(x.toString());
	}
}
