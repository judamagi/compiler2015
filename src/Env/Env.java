package Env;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import IR.Label;
import IR.Variable;
import parser.Record;
import parser.Type;

public class Env {
	public Map<String, Type> tables;
	//public Map<String, Record> structure = new HashMap<String, Record>();
	public Map<String, Record> structure;
	public ArrayList<String> defined;
	Env father;
	public int Loop = 0;
	boolean isGlobal;
	public Label SkipBlockLabel,StartBlockLabel,FunctionReturnLabel;
	public String functionName;
	private int level;
	
	
	public static Map<String, Variable> isUsed = new HashMap<String, Variable>();
	
	//--------------------------------------------
	public void changeGlobal() {
		isGlobal = false;
	}
	public Env() {
		father = null;
		isGlobal = true;
		tables = new HashMap<String, Type>();
		structure = new HashMap<>();
		defined = new ArrayList<>();
		functionName = null;
		level = 0;
	}
	public Env(Env father,boolean a, boolean b) {
		this.father = father;
		this.Loop = father.Loop; 
		isGlobal = father.isGlobal;
		if(a){
			tables = new HashMap<>();
			defined = new ArrayList<String>();
			level = father.level + 1;
		} else tables = father.tables;
		if(b)structure = new HashMap<>(); else structure = father.structure;
		SkipBlockLabel = father.SkipBlockLabel;
		StartBlockLabel = father.StartBlockLabel;
		FunctionReturnLabel = father.FunctionReturnLabel;
		functionName = father.functionName;
	}
	public boolean isGlobal() {
		return isGlobal;
	}
	public void putTables(String str, Type type) {
		tables.put(str, type);
	}
	public boolean containsKeyTables(String str) {
		Env temp = this;
		while(temp != null) {
			if(temp.tables.containsKey(str))return true;
			temp = temp.father;
		}
		return false;
	}
	public void putStructure(String str, Record re) {
		structure.put(str, re);
	}
	public boolean containsKeyStructure(String str) {
		Env temp = this;
		while(temp != null) {
			if(temp.structure.containsKey(str))return true;
			temp = temp.father;
		}
		return false;
	}
	public void addDefine(String str) {
		defined.add(str);
	}
	public boolean containsDefine(String str) {
		return defined.contains(str);
	}
	public Type getKeyTables(String str) {
		Env temp = this;
		while(temp != null) {
			if(temp.tables.containsKey(str))return temp.tables.get(str);
			temp = temp.father;
		}
		return null;
	}
	public boolean isKeyBelongGlobal(String str) {
		Env temp = this;
		while(temp != null) {
			if(temp.tables.containsKey(str))return temp.isGlobal();
			temp = temp.father;
		}
		return true;
	}
	public Record getKeyStructure(String str) {
		Env temp = this;
		while(temp != null) {
			if(temp.structure.containsKey(str))return temp.structure.get(str);
			temp = temp.father;
		}
		return null;
	}
	public int level() {
		return level;
	}
	public int getLevelByKeyTables(String valName) {
		Env temp = this;
		while(temp != null) {
			if(temp.tables.containsKey(valName))return temp.level;
			temp = temp.father;
		}
		return 0;
	}
}
