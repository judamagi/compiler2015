package error;

@SuppressWarnings("serial")
public class LexicalError extends Exception {
	public LexicalError() {}
	public LexicalError(String message){
		 super(message);
	}
}
