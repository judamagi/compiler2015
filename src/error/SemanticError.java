package error;

@SuppressWarnings("serial")
public class SemanticError extends Exception{
	public SemanticError() {}
	public SemanticError(String message) {
		super(message);
	}
}
