package SSA;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;

import error.IRError;
import IR.FunctionReturn;
import IR.FuntionStart;
import IR.Goto;
import IR.IfFalseGoto;
import IR.IfGoto;
import IR.Instruction;
import IR.Label;
import IR.PhiFunction;
import IR.Temp;
import IR.Variable;

public class ControlFlowGraph {

	String CFGName;
	private FuntionStart startIR;
	public ControlFlowGraph(FuntionStart iter) {
		startIR = iter;
		CFGName = iter.getName();
		ins = new ArrayList<Instruction>();
		corressponding = new TreeMap<>();
	}
	
	List<Instruction> ins;
	public void add(Instruction iter) {
		ins.add(iter);
	}
	
	Map<Label, Block> corressponding;
	Block start,end;
	List<Block> BlockList;
	
	Set<Variable> globals, canNotSSA;
	
	//-------------------------------------
	
	/*
	 * transfer IR to CFG
	 */
	public void  transferIRToCFG() throws IRError {
		firstWork();
		secondWork();		
	}
	
	public void collectGlobalsInfomation() {
		globals = new HashSet<Variable>();
		canNotSSA = new HashSet<Variable>();
		for (Block block : BlockList) {
			block.collectInitInfomation(globals,canNotSSA);
		}
		Iterator<Variable> i = globals.iterator();
		while(i.hasNext()){
			Variable s = i.next();
			if(s instanceof Temp || canNotSSA.contains(s) || s.isGlobal()) {
				i.remove();
			}
		}
	}
	
	public void insertPhiFunction() {		
		List<Block> workList;
		for(Variable x:globals) {
			workList = new ArrayList<Block>();
			for(Block b:x.blocks) workList.add(b);
			Block b;
			for(int i=0;i<workList.size();i++) {
				b = workList.get(i);
				for(Block d:b.DF) {
					if(d.hasNoPhiFunction(x) && d.LiveOut.contains(x)) { // <- pruned SSA
						d.addInstructionInFirst(new PhiFunction(x));
						if(!workList.contains(d))workList.add(d);
					}
				}
			}
		}
	}
	
	void secondWork() throws IRError {
		Instruction temp;
		Block x;
		for (Block block : BlockList) {
			if(block.isEmpty())continue;
			temp = block.getLastInstrcution();
			if(temp instanceof Goto) {
				x = corressponding.get(((Goto) temp).getLabel());
				block.addEdge(x);
			}
			if(temp instanceof IfFalseGoto) {
				x = corressponding.get(((IfFalseGoto) temp).getLabel());
				block.addEdge(x);
			}
			if(temp instanceof IfGoto) {
				x = corressponding.get(((IfGoto) temp).getLabel());
				block.addEdge(x);
			}			
		}
	}
	
	
	/*
	 * death instruction eliminate optimization:
	 * 		once suffer a Goto, ignore the following insturctions
	 */
	void firstWork() {
		BlockList = new ArrayList<>();
		Block t1,temp;
		start = new Block();
		BlockList.add(start);
		temp = start;
		boolean flag = true;
		for (Instruction iter : ins) {
			if(iter instanceof Label) {
				t1 = new Block((Label) iter);
				BlockList.add(t1);
				if(flag)temp.addEdge(t1);				
				temp = t1;
				corressponding.put((Label) iter, temp);
				flag = true;
			} else {
				if(!flag)continue;
				temp.addInstruction(iter);
				if(iter instanceof IfFalseGoto || iter instanceof IfGoto) {
					t1 = new Block();
					BlockList.add(t1);
					temp.addEdge(t1);				
					temp = t1;
				}
				if(iter instanceof Goto) flag = false;
			}
		}
		end = temp;
	}
	
	public void print() {
		for (Block block : BlockList) {
			block.print();
		}
	}
	
	public String getName() {
		return CFGName;
	}
	
	public void globalsAdd(Variable x) {
		globals.add(x);
	}
	
	public void calcLiveOut() {
		boolean changed = true;
		Set<Variable> temp;
		while(changed) {
			changed = false;
			for (Block block : BlockList) {
				 temp = block.computeLiveOut();
				 if(block.setNewLiveOut(temp))changed = true;
			}
		}
	}
	
	public void calcDom() {
		start.DomInitialize(false, null);
		for (Block block : BlockList) {
			if(block != start) block.DomInitialize(true, BlockList);
		}
		
		boolean changed = true;
		Set<Block> temp;
		while(changed) {
			changed = false;
			for (Block block : BlockList) {
				temp=block.computeDom();
				if(block.isDiffDom(temp)) {
					block.Dom = temp;
					changed = true;
				}
			}
		}
	}
	
	public void calcIDom() {
		Queue<Block> Q = new LinkedList<Block>();
		Q.add(start);
		start.distanceFromStart = 0;
		Block p;
		while(!Q.isEmpty()) {
			p = Q.poll();
			for(Block b:p.succ) {
				if(b.distanceFromStart == -1){
					b.distanceFromStart = p.distanceFromStart + 1;
					Q.add(b);
				}
			}
		}
		for (Block block : BlockList) {
			for(Block b: block.Dom) {
				if(b != block) {
					if(block.IDom == null || block.IDom.distanceFromStart < b.distanceFromStart) block.IDom = b;
				}
			}
		}
	}
	
	public void calcDF() {
		Block runner;
		for (Block block : BlockList) {
			if(block.pred.size() > 1) {
				for (Block p : block.pred) {
					runner =  p;
					while (runner != block.IDom) {
						runner.addDF(block);
						runner = runner.getIDom();
					}
				}
			}
		}
	}

	public void fillPhiFunction() {
		for (Variable x : globals) {
			x.reNameInitilization();
		}		
		//start.Rename(globals,BlockList);
	}

	public void extendLVN() {
		for(Block iter:BlockList) {
			iter.extendLVN();
		}
	}

	public void calcConstant() {
		for(Block iter:BlockList){
			iter.eliminateConstant();
		}
	}

	public void appendIR(List<Instruction> res) {
		res.add(startIR);
		for(Block iter:BlockList) {
			iter.appendIR(res);
		}
	}

	public void eliminateIdenticalEqution() {
		for(Block iter:BlockList){
			iter.eliminateIdenticalEqution();
		}
	}

	public void swapNumber() {
		for(Block iter:BlockList){
			iter.swapNumber();
		}
	}
}
