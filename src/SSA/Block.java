package SSA;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import IR.Assign;
import IR.BinaryOperaterAssgin;
import IR.FunctionCall;
import IR.FunctionParameter;
import IR.FunctionReturn;
import IR.Goto;
import IR.IfFalseGoto;
import IR.IfGoto;
import IR.Instruction;
import IR.IntegerConstant;
import IR.Label;
import IR.LocalVariableRequest;
import IR.Name;
import IR.PhiFunction;
import IR.PointerWrite;
import IR.SetReturnValue;
import IR.Temp;
import IR.UnaryOperatorAssign;
import IR.Variable;
import IR.VariableReName;
import IR.getAddress;
import IR.solvePointer;

public class Block {
	List<Instruction> statements;
	List<Block> succ, pred;
	
	static int count = 0;
	
	private int ID;
	public int distanceFromStart = -1; 
	public Set<Block> Dom, DF;
	public Set<Variable> UEVar,VarKill;
	public Set<Variable> LiveOut = new HashSet<>();
	Block IDom = null;
	Label label;
	
	
	//=====================================================
	
	private void __Block() {
		statements = new ArrayList<Instruction>();
		succ = new ArrayList<>();
		pred = new ArrayList<>();
		DF = new HashSet<>();
		ID = ++count ;
	}
	
	
	public Block() {
		__Block();
		this.label = null;
	}
	
	public Block(Label iter) {
		__Block();
		this.label = iter;
	}

	public void addInstruction(Instruction x) {
		statements.add(x);
	}

	public void addEdge(Block t1) {
		succ.add(t1);		
		t1.addPred(this);
	}
	
	public void addPred(Block t1) {
		pred.add(t1);
	}
	
	public Block getIDom() {
		return IDom;
	}
	
	public boolean isEmpty() {
		return statements.isEmpty();
	}
	
	public Instruction getLastInstrcution() {
		int size = statements.size();
		return statements.get(size-1);
	}

	public int getID() {
		return ID;
	}
	
	public void print() {
		if(label != null) {
			System.err.println(String.format("****Block %d(%s)****", ID, label.toString()));
		} else {
			System.err.println(String.format("****Block %d****", ID));
		}
		for (Instruction instruction : statements) {
			System.err.println(instruction.toString());
		}
		System.err.println();
		if(UEVar != null) {
			System.err.printf("\nUEVar: ");
			for(Variable v: UEVar) {
				System.err.print(v.toString()+" ");			
			}
		}
		if(VarKill != null) {
			System.err.printf("\nVarKill: ");
			for(Variable v: VarKill) {
				System.err.print(v.toString()+" ");			
			}
			System.err.println();
		}
		if(Dom != null) {
			System.err.printf("\nDom: ");
			for(Block b: Dom) {
				System.err.print(String.format("%d ", b.ID));
			}
		}
		if(DF != null) {
			System.err.printf("\nDF: ");
			for(Block b: DF) {
				System.err.print(String.format("%d ", b.ID));
			}
			System.err.println();
		}
		for (Block block : succ) {
			System.err.println(String.format("%d --> %d", ID,block.getID()));
		}
		System.err.println("****Block End****\n");
	}
	
	
	public void collectInitInfomation(Set<Variable> globals, Set<Variable> canNotSSA) {
		UEVar = new HashSet<>();
		VarKill = new HashSet<>();
		Name x,y,z;
		for (Instruction iter : statements) {
			if(iter instanceof LocalVariableRequest) {
				x = ((LocalVariableRequest) iter).getVariable();
				VarKill.add((Variable)x);
				((Variable)x).addBlocks(this);
			}
			if(iter instanceof Assign) {
				y = ((Assign) iter).y();
				if(y instanceof Variable && !VarKill.contains(y)){
					UEVar.add((Variable)y);
					globals.add((Variable)y);
				}
				x = ((Assign) iter).x();
				VarKill.add((Variable)x);
				((Variable)x).addBlocks(this);
			}
			if(iter instanceof BinaryOperaterAssgin) {
				y=((BinaryOperaterAssgin) iter).y();
				if(y!=null && !VarKill.contains(y)){
					UEVar.add((Variable)y);
					globals.add((Variable)y);
				}
				z=((BinaryOperaterAssgin) iter).z();
				if(z!=null && !VarKill.contains(z)){
					UEVar.add((Variable)z);
					globals.add((Variable)z);
				}
				x=((BinaryOperaterAssgin) iter).x();
				VarKill.add((Variable)x);
				((Variable)x).addBlocks(this);
			}
			
			if(iter instanceof UnaryOperatorAssign) {
				y=((UnaryOperatorAssign) iter).y();
				if(y!=null && !VarKill.contains(y)){
					UEVar.add((Variable)y);
					globals.add((Variable)y);
				}
				x=((UnaryOperatorAssign) iter).x();
				VarKill.add((Variable)x);
				((Variable)x).addBlocks(this);
			}
			if(iter instanceof solvePointer) {
				y=((solvePointer) iter).y();
				VarKill.add((Variable)y);
				((Variable)y).addBlocks(this);
				x=((solvePointer) iter).getX();
				VarKill.add((Variable)x);		
				((Variable)x).addBlocks(this);
				canNotSSA.add((Variable)x);
				canNotSSA.add((Variable)y);
			}
			if(iter instanceof getAddress) {
				y=((getAddress) iter).getY();
				if(y!=null && !VarKill.contains(y)){
					UEVar.add((Variable)y);
					globals.add((Variable)y);
				}
				x=((getAddress) iter).getX();
				VarKill.add((Variable)x);
				((Variable)x).addBlocks(this);
				canNotSSA.add((Variable)x);
				canNotSSA.add((Variable)y);
			}
			if(iter instanceof FunctionParameter) {
				y = ((FunctionParameter) iter).x();
				if(y!=null) {
					VarKill.add((Variable)y);
					canNotSSA.add((Variable)y);
				}
			}
			if(iter instanceof IfFalseGoto) {
				x = ((IfFalseGoto) iter).getX();
				if(x!=null && !VarKill.contains(x)){
					UEVar.add((Variable)x);
					globals.add((Variable)x);
				}
			}
			if(iter instanceof IfGoto) {
				x = ((IfGoto) iter).getX();
				if(x!=null && !VarKill.contains(x)){
					UEVar.add((Variable)x);
					globals.add((Variable)x);
				}
			}
			if(iter instanceof SetReturnValue) {
				x = ((SetReturnValue) iter).getX();
				if(x!=null && !VarKill.contains(x)){
					UEVar.add((Variable)x);
					globals.add((Variable)x);
				}
			}
		}
	}
	
	
	public Set<Variable> computeLiveOut() {
		Set<Variable> res = new HashSet<>();
		for (Block iter : succ) {
			for(Variable var: iter.UEVar){
				res.add(var);
			}
			for(Variable var: iter.LiveOut){
				if(!iter.VarKill.contains(var))res.add(var);
			}
		}
		return res;
	}

	public boolean setNewLiveOut(Set<Variable> temp) {
		boolean flag = false;
		for (Variable variable : temp) {
			if(!LiveOut.contains(variable)) {
				flag = true;
				LiveOut.add(variable);
			}
		}
		return flag;
	}
	
	
	
	public void DomInitialize(boolean isGlobal, List<Block> globalBlocks) {
		Dom = new HashSet<>();
		if(isGlobal){
			for (Block block : globalBlocks) {
				Dom.add(block);
			}
		} else {			
			Dom.add(this);
		}
	}
	
	public Set<Block> computeDom() {
		Set<Block> res = new HashSet<>(), temp;
		boolean flag = true;
		for (Block predict : pred) {
			if(flag) {
				flag = false;
				for(Block iter:predict.Dom) {
					res.add(iter);
				}
			} else {
				temp = new HashSet<>();
				for(Block iter:predict.Dom) {
					if(res.contains(iter))temp.add(iter);
				}
				res = temp;
			}
		}
		res.add(this);
		return res;
	}

	public boolean isDiffDom(Set<Block> temp) {
		if(temp.size() != Dom.size()) return true;
		for(Block iter:temp){
			if(!Dom.contains(iter)) return  true;
		}
		return false;
	}

	public void setIDom(Block iter) {
		IDom = iter;
	}

	public void addDF(Block block) {
		DF.add(block);
	}

	public boolean hasNoPhiFunction(Variable x) {
		for (Instruction iter : statements) {
			if(iter instanceof PhiFunction && ((PhiFunction) iter).getVariable() == x) return false;
		}
		return true;
	}

	public void addInstructionInFirst(PhiFunction phiFunction) {
		statements.add(0, phiFunction);		
	}


	public void eliminateConstant() {
		int len = statements.size();
		Instruction iter;
		for(int i=0;i<len;i++) {
			iter = statements.get(i);
			if(iter instanceof BinaryOperaterAssgin) {
				Name y,z;
				y = iter.y();
				z = iter.z();
				if(y instanceof IntegerConstant && z instanceof IntegerConstant) {
					int a = (int) ((IntegerConstant) y).getValue();
					int b = (int) ((IntegerConstant) z).getValue();
					int c = 0;
					String op = ((BinaryOperaterAssgin) iter).getOperator();
					if(op.equals("+")) c = a+b;
					if(op.equals("-")) c = a-b;
					if(op.equals("*")) c = a*b;
					if(op.equals("/")) c = a/b;
					if(op.equals("<<")) c = a<<b;
					if(op.equals(">>")) c = a>>b;
					if(op.equals(">")) c = a>b?1:0;
					if(op.equals(">=")) c = a>=b?1:0;
					if(op.equals("<")) c = a<b?1:0;
					if(op.equals("<=")) c = a<=b?1:0;
					if(op.equals("==")) c = a==b?1:0;
					if(op.equals("!=")) c = a!=b?1:0;
					if(op.equals("%")) c = a%b;
					if(op.equals("&")) c = a&b;
					if(op.equals("|")) c = a|b;
					if(op.equals("^")) c = a^b;
					statements.set(i, new Assign((Variable) iter.x(), new IntegerConstant(c), 4));
				}
			}
			if(iter instanceof UnaryOperatorAssign) {
				Name y = iter.y();
				if(y instanceof IntegerConstant ) {
					int a = (int) ((IntegerConstant) y).getValue(),b = 0;
					String op = ((UnaryOperatorAssign) iter).getOperator();
					if(op.equals("+")) b = a;
					if(op.equals("-")) b = -a;
					if(op.equals("~")) b = ~a;
					if(op.equals("!")) b = a==0?1:0;
					statements.set(i,new Assign((Variable) iter.x(), new IntegerConstant(b), 4));
				}
			}
		}
	}


	public void appendIR(List<Instruction> res) {
		if(label != null) res.add(label);
		for(Instruction iter:statements) {
			res.add(iter);
		}
	}


	public void eliminateIdenticalEqution() {
		int len = statements.size();
		Instruction iter;
		for(int i=0;i<len;i++) {
			iter = statements.get(i);
			if(iter instanceof BinaryOperaterAssgin) {
				Name y,z;
				y = iter.y();
				z = iter.z();
				String op = ((BinaryOperaterAssgin) iter).getOperator();
				if(y instanceof Variable && z instanceof IntegerConstant) {
					int u = (int)((IntegerConstant) z).getValue();
					if(op.equals("+") && u == 0) statements.set(i, new Assign((Variable) iter.x(), y, 4));
					if(op.equals("-") && u == 0) statements.set(i, new Assign((Variable) iter.x(), y, 4));
					if(op.equals("*") && u == 1) statements.set(i, new Assign((Variable) iter.x(), y, 4));
					if(op.equals("/") && u == 1) statements.set(i, new Assign((Variable) iter.x(), y, 4));
					if(op.equals("*") && u == 0) statements.set(i, new Assign((Variable) iter.x(), new IntegerConstant(0), 4));
				}
				if(z instanceof Variable && y instanceof IntegerConstant) {
					int u = (int)((IntegerConstant) y).getValue();
					if(op.equals("+") && u == 0) statements.set(i, new Assign((Variable) iter.x(), z, 4));
					if(op.equals("*") && u == 1) statements.set(i, new Assign((Variable) iter.x(), z, 4));
					if(op.equals("*") && u == 0) statements.set(i, new Assign((Variable) iter.x(), new IntegerConstant(0), 4));
				}
			}			
		}
		Iterator<Instruction> it = statements.iterator();
		while(it.hasNext()) {
			iter = it.next();
			if(iter instanceof Assign && iter.x() == iter.y()) it.remove();
		}
	}

	private int LVNHashCode(String op,int y,int z) {
		int t1 = Math.max(z, y);
		int t2 = Math.min(y, z);
		return op.hashCode()+t1*10000+t2;
	}

	public void extendLVN() {
		Map<Integer, Integer> hash = new HashMap<>();
		Map<Name, Integer> order = new HashMap<>();
		int count = 0;
		int len = statements.size();
		Instruction iter;
		for(int i = 0;i < len; i++) {
			iter = statements.get(i);
			if(iter instanceof Assign || iter instanceof solvePointer || iter instanceof UnaryOperatorAssign
					|| iter instanceof getAddress) {
				order.put(iter.x(), count++);
			}
			if(iter instanceof BinaryOperaterAssgin) {
				Name x = iter.x(), y = iter.y(), z = iter.z();
				int t1,t2,t3;
				if(order.containsKey(y)) t1 = order.get(y); else  {t1 = count++; order.put(y, t1);}
				if(order.containsKey(z)) t2 = order.get(z); else  {t2 = count++; order.put(z, t2);}
				String op = ((BinaryOperaterAssgin) iter).getOperator();
				int opcode = LVNHashCode(op, t1, t2);
				if(hash.containsKey(opcode)) {
					t3 = hash.get(opcode);
					for(Entry<Name, Integer> iter1:order.entrySet()) {
						if(iter1.getValue() == t3) {
							Name cd = iter1.getKey();
							statements.set(i, new Assign((Variable) x, cd, 4));
							break;
						}
					}
				} else {
					t3 = count++;
					order.put(x, t3);
					hash.put(opcode, t3);
				}
			}
			if(iter instanceof PointerWrite) {
				for(Entry<Name, Integer> it:order.entrySet()){
					it.setValue(count++);
				}
			}
		}
	}


	public void swapNumber() {
		for(Instruction it:statements) {
			if(it instanceof BinaryOperaterAssgin){
				Name y = it.y(), z = it.z();
				if(y instanceof IntegerConstant && !(z instanceof IntegerConstant)){
					String cd = ((BinaryOperaterAssgin) it).getOperator();
					if(cd.equals("+") || cd.equals("*"))
						((BinaryOperaterAssgin)it).swapYZ();
				}
			}
		}
	}
		
	

	/*public void Rename(Set<Variable> globals, List<Block> blockList) {
		Variable x,y,u,z;
		for (Instruction iter : statements) {
			if(iter instanceof PhiFunction) {
				((PhiFunction) iter).reNameX();
			}
			if(iter instanceof LocalVariableRequest) {
				x = ((LocalVariableRequest) iter).getVariable();
				if(x!=null && globals.contains(x)) ((LocalVariableRequest) iter).ReName();
			}
			if(iter instanceof Assign) {
				x = ((Assign) iter).x();
				if(x!=null && globals.contains(x)) ((Assign) iter).ReNameX();
				y = ((Assign) iter).y();
				if(y!=null && globals.contains(y)) ((Assign) iter).ReNameY();
			}
			if(iter instanceof BinaryOperaterAssgin) {
				x = ((BinaryOperaterAssgin) iter).getX();
				if(x!=null && globals.contains(x)) ((BinaryOperaterAssgin) iter).ReNameX();
				y = ((BinaryOperaterAssgin) iter).getY();
				if(y!=null && globals.contains(y)) ((BinaryOperaterAssgin) iter).ReNmaeY();
				z = ((BinaryOperaterAssgin) iter).getZ();
				if(z!=null && globals.contains(z)) ((BinaryOperaterAssgin) iter).ReNameZ();
			}
			if(iter instanceof IfFalseGoto) {
				x = ((IfFalseGoto) iter).getX();
				if(x!=null && globals.contains(x)) ((IfFalseGoto) iter).ReName();
			}
			if(iter instanceof IfGoto) {
				x = ((IfGoto) iter).getX();
				if(x!=null && globals.contains(x)) ((IfGoto) iter).ReName();
			}
			if(iter instanceof SetReturnValue) {
				x = ((SetReturnValue) iter).getX();
				if(x!=null && globals.contains(x)) ((SetReturnValue) iter).ReName();
			}
			if(iter instanceof UnaryOperatorAssign) {
				x = ((UnaryOperatorAssign) iter).getX();
				if(x!=null && globals.contains(x)) ((UnaryOperatorAssign) iter).ReNameX();
				y = ((UnaryOperatorAssign) iter).getY();
				if(y!=null && globals.contains(y)) ((UnaryOperatorAssign) iter).ReNameY();
			}
		}
		for(Block s:succ) {
			for(Instruction iter: s.statements) {
				if(iter instanceof PhiFunction) {
					x = ((PhiFunction) iter).getVariable();
					((PhiFunction) iter).addParameter(x.TopName());
				} else break;
			}
		}
		for(Block s:blockList) {
			if(s.IDom == this) 
				s.Rename(globals, blockList);
		}
		for (Instruction iter : statements) {
			if(iter instanceof PhiFunction) {
				x = ((PhiFunction) iter).getVariable();
				x.popName();
			}
			if(iter instanceof Assign) {
				x = ((Assign) iter).getX();
				if(x!=null && globals.contains(x)) x.popName();
			}
			if(iter instanceof BinaryOperaterAssgin) {
				x = ((BinaryOperaterAssgin) iter).getX();
				if(x!=null && x instanceof VariableReName && globals.contains(((VariableReName) x).ancestor())) x.popName();
			}
			if(iter instanceof UnaryOperatorAssign) {
				x = ((UnaryOperatorAssign) iter).getX();
				if(x!=null && x instanceof VariableReName && globals.contains(((VariableReName) x).ancestor())) x.popName();
			}
		}
	}*/
}
