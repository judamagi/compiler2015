package SSA;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import error.IRError;
import IR.Instruction;

public class StaticSingleAssignmentForm {
	
	public static void work(){
		try {
			List<ControlFlowGraph> CFGs = Instruction.createCFG();
			for (ControlFlowGraph controlFlowGraph : CFGs) {
				controlFlowGraph.transferIRToCFG();
//				print(controlFlowGraph);
//				controlFlowGraph.collectGlobalsInfomation();
//				controlFlowGraph.calcDom();
//				controlFlowGraph.calcIDom();
//				controlFlowGraph.calcDF();
//				controlFlowGraph.calcLiveOut();
//				controlFlowGraph.collectGlobalsInfomation();
//				controlFlowGraph.insertPhiFunction();
//				controlFlowGraph.fillPhiFunction();
//				print(controlFlowGraph);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void print(ControlFlowGraph controlFlowGraph) {		
		System.err.println("====Control Flow Graph ("+controlFlowGraph.getName()+") Infomation====");
		controlFlowGraph.print();
	}
}
