package parser;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import Env.Env;
import IR.FunctionCall;
import IR.FunctionParameter;
import IR.FunctionSignal;
import IR.Instruction;
import IR.Name;
import IR.Temp;
import error.SemanticError;

public class CallExpr extends Stmt {
	public CallExpr(IdentifierExpr temp) {
		functionName = temp;
	}
	IdentifierExpr functionName;
	ArrayList<Stmt> params = new ArrayList<Stmt>();
	public void add(Stmt param) {
		params.add(param);
	}
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<CallExpr >\n",blank);
		functionName.show(indent+1);
		for (Stmt stmt : params) {
			stmt.show(indent+1);
		}
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		if(!env.containsKeyTables(functionName.toString())) throw new SemanticError("undefined function");
		Type ty = env.getKeyTables(functionName.toString());
		if(!(ty instanceof Function)) throw new SemanticError("no a function");
		Function foo = (Function) ty;
		if(foo.paramType.size() != params.size()) throw new SemanticError("uncorresponding amount of parameters");
		List<Name> parameters = new LinkedList<>();
		List<Integer> paramSize = new LinkedList<>();
		for(int i=0;i<params.size();++i ) {
			annotated cc = params.get(i).check(env);
			if(cc.type instanceof Array) paramSize.add(4); else	paramSize.add(cc.type.calcSize(env));
			Type.matchAssign(foo.paramType.get(i), cc.type);
			parameters.add(cc.name);
		}
		Instruction.add(new FunctionSignal(functionName.identifier,params.size()));
		for (int i=0;i<parameters.size();++i) {
			Instruction.add(new FunctionParameter(parameters.get(i),paramSize.get(i)));
		}
		Temp temp = new Temp();
		Instruction.add(new FunctionCall(functionName.identifier,temp,foo.returnType.getConstSpace()));
		return new annotated(foo.returnType, false,temp);
	}
}
