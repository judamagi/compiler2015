package parser;

import java.util.List;

import error.*;
import Env.Env;
import IR.Instruction;
import IR.Name;

public abstract class AbstractSyntaxTree {
	public abstract void show(int indent);
	public abstract annotated check(Env env) throws SemanticError;
}