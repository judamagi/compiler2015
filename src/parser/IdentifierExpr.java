package parser;



public abstract class IdentifierExpr extends Stmt {
	public IdentifierExpr(String identifierStr) {
		// TODO Auto-generated constructor stub
		identifier = identifierStr;
	}

	String identifier;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<IdentifierExpr: %s>\n",blank,identifier);
	}
	public String toString() {
		return identifier;
	}
}
