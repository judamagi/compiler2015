package parser;

import Env.Env;
import IR.BinaryOperaterAssgin;
import IR.Instruction;
import IR.IntegerConstant;
import IR.Name;
import IR.Temp;
import IR.Variable;
import IR.solvePointer;
import error.SemanticError;

public class MemberExpr extends Stmt {
	// -> expression
	// Owner->member;
	Stmt Owner;
	String member;
	String Op;
	public MemberExpr(Stmt res, String temp, String identifier) {
		Owner = res;
		Op = temp;
		member = identifier;
	}
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<MemberExpr: %s>\n",blank,Op);
		Owner.show(indent+1);
		System.err.printf("%s\t%s",member);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		annotated res = Owner.check(env);
		if(Op.equals("->")) {			
			if(!(res.type instanceof Pointer))throw new SemanticError("owner isn't a pointer");
			Type temp = ((Pointer) res.type).getRealType(env);
			if(!(temp instanceof Record)) throw new SemanticError("owner isn't a record");			
			if(((Record)temp).elements.containsKey(member)){
				Type tt = ((Record)temp).elements.get(member);
				Name offset = ((Record)temp).calcOffset(member,env);				
				Variable s = new Temp(), s1 = new Temp();
				Instruction.add(new BinaryOperaterAssgin(s1, res.name, offset, "+"));
				if(tt instanceof Array) {
					return new annotated(tt, false, s1);
				} else {
					Instruction.add(new solvePointer(s, s1,tt.calcSize(env)));
					return new annotated(tt, true,s);
				}
			}
		} else {
			if(!(res.type instanceof Record))throw new SemanticError("owner isn't a Record");			
			Type temp = res.type;		
			if(((Record)temp).elements.containsKey(member)){
				Type tt = ((Record)temp).elements.get(member);
				Name offset = ((Record)temp).calcOffset(member,env);
				Variable s = new Temp(),s1 = new Temp();
				Instruction lastInstruction = Instruction.getLastInstruction();			
				if(lastInstruction instanceof solvePointer && ((solvePointer) lastInstruction).x() == res.name) {
					Name y = ((solvePointer)lastInstruction).y();
					Instruction.removeLastInstruction();
					Instruction.add(new BinaryOperaterAssgin(s1,y, offset,"+"));
				} else {
					Instruction.add(new BinaryOperaterAssgin(s1,res.name, offset,"+"));					
				}							
				if(tt instanceof Array) {
					return new annotated(tt, false, s1);
				} else {
					Instruction.add(new solvePointer(s, s1,tt.calcSize(env)));
					return new annotated(tt, true,s);
				}				
			}
		}
		return null;
	}
	
	
}
