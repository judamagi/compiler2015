package parser;

import Env.Env;
import IR.Goto;
import IR.IfFalseGoto;
import IR.Instruction;
import IR.Label;
import error.SemanticError;

public class WhileStmt extends Stmt {
	public WhileStmt(Stmt expr, Stmt statement2) {
		// TODO Auto-generated constructor stub
		condition = expr;
		statement = statement2;
	}

	Stmt condition,statement;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<WhileStmt>\n",blank);
		condition.show(indent+1);
		statement.show(indent+1);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		Env subenv = new Env(env,true,true);
		Label l1 = new Label(), l2 = new Label();
		subenv.StartBlockLabel = l1;
		subenv.SkipBlockLabel = l2;
		Instruction.add(l1);
		annotated res;
		try {
			res = condition.check(env); 
			Type.matchAssign(Parser.INT,res.type);
		} catch (SemanticError e) {
			throw new SemanticError("while condition must return type int");
		}		
		Instruction.add(new IfFalseGoto(res.name, l2));
		subenv.Loop += 1;
		statement.check(subenv);
		subenv.Loop -= 1;
		Instruction.add(new Goto(l1));
		Instruction.add(l2);
		return null;
	}
}
