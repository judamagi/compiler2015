package parser;

import java.util.ArrayList;
import java.util.List;

import Env.Env;
import IR.Instruction;
import IR.Name;
import error.SemanticError;

public class DeclStmt extends Stmt {
	public DeclStmt(ArrayList<Decl> parseDeclaration) {
		// TODO Auto-generated constructor stub
		declartion = parseDeclaration;
	}

	ArrayList<Decl> declartion;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<DeclStmt>\n",blank);
		for (Decl decl : declartion) {
			decl.show(indent+1);
		}
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		for (Decl decl : declartion) {
			decl.check(env);
		}
		return null;
	}
}
