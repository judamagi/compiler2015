package parser;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import Env.Env;
import Env.StaticData;
import IR.Assign;
import IR.BinaryOperaterAssgin;
import IR.Instruction;
import IR.IntegerConstant;
import IR.LocalVariableRequest;
import IR.Name;
import IR.PointerWrite;
import IR.Temp;
import IR.Variable;
import error.SemanticError;

public class VarDecl extends Decl {
	IdentifierExpr varName;
	String type;
	Stmt expr=null;
	public VarDecl(String type,IdentifierExpr varName) {
		this.type = type;
		this.varName = varName;
	}
	
	public VarDecl(String type,IdentifierExpr name, Stmt expr) {
		this.type = type;
		this.varName = name;
		this.expr = expr;
	}
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<VarDecl: type %s name: %s>\n",blank,type.toString(),varName);
		if(expr!=null)expr.show(indent+1);
	}

	@Override 
	public annotated check(Env env) throws SemanticError {
		Type baseType;
		String temp = new String(varName.identifier);
		if(temp.startsWith("*")){
			baseType = new Pointer(type);
			temp=temp.substring(1);
			while(temp.startsWith("*")){
				baseType = new Pointer(baseType);
				temp=temp.substring(1);
			}
		} else {		
			if(type.equals("int")) baseType = Parser.INT; else
			if(type.equals("void")) throw new SemanticError("void type rejected:"+varName.identifier); else
			if(type.equals("char")) baseType = Parser.CHAR; else {
				if(!env.containsKeyStructure(type))throw new SemanticError("undefined record");
				baseType = env.getKeyStructure(type);
			}
		}
		if(varName instanceof IdentifierArray){
			for (Stmt stmt : ((IdentifierArray) varName).expression) {
				if(env.isGlobal()) {					
					int x = Stmt.calcConstantExpression(stmt, env);
					baseType = new Array(baseType, new IntegerLiteral(x));
				} else {					
					try {
						int x = Stmt.calcConstantExpression(stmt, env);
						if(x<0) throw new SemanticError("cant' negative array size"); else baseType = new Array(baseType, new IntegerLiteral(x));
					} catch (Exception e) {
						if(e.getMessage() == "cant' negative array size") throw e;
						stmt.check(env);
						baseType = new Array(baseType, stmt);
					}
				}
			}
		}
		
		baseType.calcSize(env);
		
		
		if(env.isGlobal()) {
			Type tt = env.tables.get(temp);
			if(tt == null) {
				env.tables.put(temp, baseType);
				if(expr!=null)env.defined.add(temp);
				StaticData.addVariable(temp,baseType,baseType.getConstSpace());
			} else {
				if(tt instanceof Array) throw new SemanticError("conflicting type "+temp);
				if(!tt.typeEquals(baseType))throw new SemanticError("different type:"+temp);
				if(expr!=null ){
					if(env.defined.contains(temp))throw new SemanticError("redefined variable in global namespace"); 
						else env.defined.add(temp);
				}
			}
			if(expr != null) {
				if(baseType instanceof Record) throw new SemanticError(baseType.toString()+"can't be initialize");
				if(baseType instanceof Pointer && ((Pointer) baseType).getRealType(env) instanceof CHAR) {
					if(!(expr instanceof StringLiteral || checkStringInitialize(expr,env)))throw new SemanticError("error string initialize");
					if(expr instanceof StringLiteral) StaticData.update(temp,((StringLiteral) expr).getValue()); 
						else StaticData.update(temp, calcStringInitialize(expr,env),true);
				} else {
					if(baseType instanceof Array) {
						checkInitialize(baseType,env,expr);
						Type inherentType = ((Array) baseType).getRealType(env);
						int cc = baseType.getConstSpace()/4;
						List<Object> x = new ArrayList<>(cc+10);
						for(int i=0;i<cc;++i)x.add(0);
						MIPSArrayInitialize(baseType, env, expr, x,0);
						StaticData.update(temp, x,baseType);
					} else {						
						StaticData.update(temp, checkScalarInitialize(expr,env));
					}
				}
				
			}
		} else {
			if(env.tables.containsKey(temp))throw new SemanticError("redefined variable");
			env.tables.put(temp, baseType);
			String nameStr = temp+"#"+env.functionName+String.valueOf(env.level());
			Variable localName = new Variable(nameStr,baseType instanceof Record,baseType instanceof Array);
			Env.isUsed.put(nameStr, localName);
			Instruction.add(new LocalVariableRequest(localName, baseType.getConstSpace()));
			if(expr != null) {
				if(baseType instanceof Array){
					if(((Array) baseType).getRealType(env) instanceof CHAR && expr instanceof StringLiteral){
						Name t = expr.check(env).name;
						int len =((StringLiteral) expr).getValue().length()+1;
						Instruction.add(new Assign(localName, t,len));						
					} else {
						checkInitialize(baseType,env,expr); 
						IRArrayInitialize(baseType,env,expr,localName,0);
					}
				} else {
					annotated t = expr.check(env);
					Instruction.add(new Assign(localName, t.name,t.type.calcSize(env)));
				}
			}
		}		
		return null;
	}

	private void MIPSArrayInitialize(Type baseType, Env env,
			Stmt expr, List<Object> x, int location) throws SemanticError {
		if(expr instanceof CompoundStmt) {
			Type inherentType = ((Array) baseType).getRealType(env);
			int t = inherentType.getConstSpace()/4;
			int i = 0;
			for(Stmt iter:((CompoundStmt) expr).compoundStmtList) {
				MIPSArrayInitialize(inherentType, env, iter, x, i+location);
				i += t;
			}
		} else { 
			if(expr instanceof StringLiteral){
				String str = ((StringLiteral) expr).getValue();
				for(int i=0;i<str.length();i++){
					x.set(location+i, str.charAt(i));
				}
			} else {
				x.set(location,Stmt.calcConstantExpression(expr, env));
			}
		}
	}

	private List<Character> calcStringInitialize(Stmt expr, Env env) {
		// TODO
		return null;
	}

	private void IRArrayInitialize(Type baseType, Env env, Stmt expr,
			Variable localName,int location) throws SemanticError {
		if(expr instanceof CompoundStmt){
			Type inherentType = ((Array) baseType).getRealType(env);
			for (Stmt st : ((CompoundStmt)expr).compoundStmtList) {
				if(inherentType instanceof Array) {
					IRArrayInitialize(inherentType, env, st, localName,location);
				} else {
					annotated tt = st.check(env);
					Variable temp = new Temp();
					Instruction.add(new BinaryOperaterAssgin(temp, localName, new IntegerConstant(location), "+"));
					Instruction.add(new PointerWrite(temp, tt.name,inherentType.calcSize(env)));
				}
				location += inherentType.getConstSpace();
			}
		}
	}

	private boolean checkStringInitialize(Stmt expr,Env env) throws SemanticError {
		if(expr instanceof CompoundStmt) {
			if(((CompoundStmt) expr).compoundStmtList.size() == 0) return false;
			for (Stmt iter : ((CompoundStmt) expr).compoundStmtList) {
				if(!checkStringInitialize(iter,env))return false;
			}
			return true;
		} else {
			try {
				Stmt.calcConstantExpression(expr, env);
				return true;
			} catch (Exception e){
				return expr instanceof StringLiteral;
			}
		}
	}

	private int checkScalarInitialize(Stmt expr, Env env) throws SemanticError {
		if(expr instanceof CompoundStmt) {
			int temp=((CompoundStmt) expr).compoundStmtList.size();
			if(temp == 0 )throw new SemanticError("empty  initialize");
			for(int i=1;i<temp;i++)((CompoundStmt) expr).compoundStmtList.get(i).check(env);
			return checkScalarInitialize(((CompoundStmt) expr).compoundStmtList.get(0), env);
		} else return Stmt.calcConstantExpression(expr, env);
	}

	private void checkInitialize(Type type,Env env, Stmt expr) throws SemanticError {
		if(!(expr instanceof CompoundStmt))throw new SemanticError("error initialize");
		ArrayList<Stmt> temp = ((CompoundStmt)expr).compoundStmtList;
		//if(temp.size() == 0) throw new SemanticError("empty initialize");
		for (Stmt stmt : temp) {
			if(type instanceof Array) {
				if(((Array) type).elemType instanceof CHAR) {
					if(!checkStringInitialize(stmt, env))throw new SemanticError("error array define");
					continue;
				} 
				if(stmt instanceof CompoundStmt) {
					checkInitialize(((Array) type).elemType, env, stmt);
					continue;
				} else if(stmt instanceof StringLiteral) {
					if(((Array) type).elemType instanceof Pointer){
						Pointer tt = (Pointer) ((Array) type).elemType;
						if(tt.getRealType(env) instanceof CHAR)continue;
					}
					if(((Array) type).getRealType(env) instanceof CHAR) continue;
					throw new SemanticError("error char array initialize");
				} else {
					if(env.isGlobal()){
						stmt.calcConstantExpression(stmt, env);
					} else {
						type.matchAssign(type, stmt.check(env).type);
					}
				}
				
			} else {
				if(env.isGlobal()){
					stmt.calcConstantExpression(expr, env);
				} else {
					type.matchAssign(type, expr.check(env).type);
				}
			}
		}
	}
}
