package parser;

import Env.Env;
import IR.IntegerConstant;
import IR.Name;

final public class CHAR extends Type {
	@Override
	public String toString() {
		return "char";
	}

	@Override
	public boolean typeEquals(Type baseType) {
		return baseType instanceof CHAR;
	}

	@Override
	int calcSize(Env env) {
		return 1;
	}

	@Override
	public
	int getConstSpace() {
		return 1;
	}

	@Override
	public int getRealSize(Env env) {
		return 1;
	}
}
