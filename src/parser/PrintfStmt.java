package parser;

import java.util.ArrayList;
import java.util.List;

import Env.Env;
import Env.StaticData;
import IR.FunctionParameter;
import IR.FunctionSignal;
import IR.Instruction;
import IR.Name;
import IR.__Printf__;
import error.SemanticError;

public class PrintfStmt extends Stmt {
	public PrintfStmt(String fotmat2, ArrayList<Stmt> lists) {
		fotmat = fotmat2;
		elements = lists;
	}
	String fotmat,id;
	ArrayList<Stmt> elements;
	static int count = 0;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<PrintfStmt: fotmat %s>\n",blank,fotmat);
		for (Stmt stmt : elements) {
			stmt.show(indent+1);
		}
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		id = StaticData.addStringConstant(fotmat);
		List<Name> nameList = new ArrayList<>();
		List<Integer> nameSize = new ArrayList<>();
		annotated x;
		for (Stmt stmt : elements) {
			x = stmt.check(env);
			nameList.add(x.name);
			nameSize.add(x.type.calcSize(env));
		}
		Instruction.add(new FunctionSignal("printf", elements.size()));
		for (int i=0;i<nameList.size();++i) {
			Instruction.add(new FunctionParameter(nameList.get(i),nameSize.get(i)));
		}
		Instruction.add(new __Printf__(id,nameList.size()));
		return new annotated(Parser.INT, false,null);
	}
}
