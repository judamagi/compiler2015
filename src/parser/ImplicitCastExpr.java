package parser;

import Env.Env;
import IR.Assign;
import IR.Instruction;
import IR.Name;
import IR.Temp;
import IR.Variable;
import error.SemanticError;

public class ImplicitCastExpr extends Stmt {
	Type upToType;
	Stmt statement;
	String type;
	public ImplicitCastExpr(String type, Stmt parsePrimary) {
		this.type = type;
		statement = parsePrimary;
	}
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<ImplicitCastExpr: type %s>\n",blank,upToType);
		statement.show(indent+1);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		annotated r =statement.check(env);
		Type res = r.type;
		Type baseType;
		String temp = new String(type);
		if(temp.startsWith("*")){
			baseType = new Pointer(type);
			temp=temp.substring(1);
			while(temp.startsWith("*")){
				baseType = new Pointer(baseType);
				temp=temp.substring(1);
			}
		} else {		
			if(type.equals("int")) baseType = Parser.INT; else
			if(type.equals("void")) throw new SemanticError("void type rejected"); else
			if(type.equals("char")) baseType = Parser.CHAR; else {
				if(!env.containsKeyStructure(type))throw new SemanticError("undefined record");
				baseType = env.getKeyStructure(type);
			}
		}
		upToType = baseType;
		Type.matchAssign(upToType, res);
		Variable s = new Temp();
		Instruction.add(new Assign(s, r.name,Math.min(upToType.calcSize(env), res.calcSize(env))));
		return new annotated(upToType, false,s);
	}
}
