package parser;

public abstract class Literal extends Stmt {
	abstract public Object getValue();
}
