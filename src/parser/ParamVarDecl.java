package parser;

import Env.Env;
import IR.Instruction;
import IR.LocalVariableRequest;
import IR.Name;
import IR.Variable;
import error.SemanticError;

public class ParamVarDecl extends Decl {
	public ParamVarDecl(String paramType, IdentifierExpr name) {
		type = paramType;
		paramName = name;
	}
	public IdentifierExpr paramName;
	String type;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<ParamVarDecl: type %s name: %s>\n",blank,type.toString(),paramName);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		Type baseType;
		String temp = new String(paramName.identifier);
		if(temp.startsWith("*")){
			baseType = new Pointer(type);
			temp=temp.substring(1);
			while(temp.startsWith("*")){
				baseType = new Pointer(baseType);
				temp=temp.substring(1);
			}
		} else {		
			if(type.equals("int")) baseType = Parser.INT; else
			if(type.equals("void")) throw new SemanticError("void type rejected"); else
			if(type.equals("char")) baseType = Parser.CHAR; else {
				if(!env.containsKeyStructure(type))throw new SemanticError("undefined record");
				baseType = env.getKeyStructure(type);
			}
		}
		if(paramName instanceof IdentifierArray){
			for (Stmt stmt : ((IdentifierArray) paramName).expression) {
				if(env.isGlobal()) {					
					int x = Stmt.calcConstantExpression(stmt, env);
					baseType = new Array(baseType, new IntegerLiteral(x));
				} else {					
					try {
						int x = Stmt.calcConstantExpression(stmt, env);
						if(x<0) throw new SemanticError("cant' negative array size"); else baseType = new Array(baseType, new IntegerLiteral(x));
					} catch (Exception e) {
						if(e.getMessage() == "cant' negative array size") throw e;
						stmt.check(env);
						baseType = new Array(baseType, stmt);
					}
				}
			}
		}
		baseType.calcSize(env);
		env.tables.put(temp, baseType);
		String NameStr = new String(temp+"#"+env.functionName+String.valueOf(env.getLevelByKeyTables(temp))); 
		Variable paramName = new Variable(NameStr,baseType instanceof Record,baseType instanceof Array);
		Env.isUsed.put(NameStr, paramName);
		int number = baseType.calcSize(env);
		if(baseType instanceof Array || baseType instanceof CHAR) number = 4;
		Instruction.add(new LocalVariableRequest(paramName, number));
		return new annotated(baseType, false, paramName);
	}
}
