package parser;

import Env.Env;
import IR.Assign;
import IR.BinaryOperaterAssgin;
import IR.Instruction;
import IR.IntegerConstant;
import IR.Name;
import IR.PointerWrite;
import IR.Temp;
import IR.UnaryOperatorAssign;
import IR.Variable;
import IR.solvePointer;
import error.SemanticError;

public class SuffixOperator extends Stmt {

	public SuffixOperator(String opStr, Stmt res) {
		Op = opStr;
		statement = res;
	}
	@Override
	public void show(int indent) {
		// TODO Auto-generated method stub
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<SuffixOperator :%s>\n",blank,Op);
		statement.show(indent+1);
	}
	String Op;
	Stmt statement;
	@Override
	public annotated check(Env env) throws SemanticError {
		annotated res = statement.check(env);
		if(!res.isLeftValue) throw new SemanticError("require a left value");
		Variable temp = new Temp();
		Instruction lastInstruction = Instruction.getLastInstruction();
		Name q = null;
		if(lastInstruction instanceof solvePointer && lastInstruction.x() == res.name) {
			q = lastInstruction.y();
		} 
		Instruction.add(new Assign(temp, res.name,4));
		Instruction.add(new BinaryOperaterAssgin((Variable) res.name, res.name, new IntegerConstant(1), Op.substring(1)));
		if(lastInstruction instanceof solvePointer && lastInstruction.x() == res.name) {
			Instruction.add(new PointerWrite(q, res.name,4));
		}
		return new annotated(res.type, false,temp);
	}
}
