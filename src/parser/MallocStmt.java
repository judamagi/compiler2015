package parser;

import Env.Env;
import IR.Instruction;
import IR.Temp;
import IR.__malloc__;
import error.SemanticError;

public class MallocStmt extends Stmt {
	public MallocStmt(Stmt expr) {
		size_t = expr;
	}

	Stmt size_t;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<MallocStmt>\n",blank);
		size_t.show(indent+1);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		annotated res = size_t.check(env);
		Type.matchAssign(Parser.INT,res.type);
		Temp x = new Temp();
		Instruction.add(new __malloc__(x, res.name));
		return new annotated(new Pointer(Parser.VOID), false, x);
	}
}
