package parser;

import Env.Env;
import IR.IntegerConstant;
import IR.Name;
import error.SemanticError;

public class SizeofOperator extends Stmt {

	public SizeofOperator(Stmt parseIdentifierExpr) {
		expr = parseIdentifierExpr;
		selete = 0;
	}

	public SizeofOperator(String type) {
		this.type = type;
		selete = 1;
	}

	@Override
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<SizeofOperator:>\n",blank);
		if(selete ==0 )	expr.show(indent+1); else System.err.printf("%s\t%s\n",blank,type.toString());
	}
	
	Stmt expr;
	String type;
	Type realType;
	int selete;
	@Override
	public annotated check(Env env) throws SemanticError {
		if(selete ==0 ){
			annotated res = expr.check(env);
			if(res==null)throw new SemanticError("sizeof error!");
			realType = res.type;
		} else {
			Type baseType;
			String temp = new String(type);
			if(temp.startsWith("*")){
				baseType = new Pointer(type);
				temp=temp.substring(1);
				while(temp.startsWith("*")){
					baseType = new Pointer(baseType);
				}
			} else {		
				if(type.equals("int")) baseType = Parser.INT; else
				if(type.equals("void")) throw new SemanticError("void type rejected"); else
				if(type.equals("char")) baseType = Parser.CHAR; else {
					if(!env.containsKeyStructure(type))throw new SemanticError("undefined record");
					baseType = env.getKeyStructure(type);
				}
			}
			realType = baseType;
		}
		Name size = new IntegerConstant(realType.getRealSize(env));
		return new annotated(Parser.INT, false,size);
	}
	
	int getConstSize(Env env) throws SemanticError {
		if(selete == 1){
			if(type.equals("int")) return 4;
			if(type.equals("char")) return 1;
			if(type.equals("void")) return 1;
			if(type.startsWith("struct") || type.startsWith("union")) return env.getKeyStructure(type).getConstSpace();
			throw new SemanticError("unknown type:"+type);
		}
		annotated temp = expr.check(env);
		if(temp==null) throw new SemanticError("sizeof error");
		return temp.type.getRealSize(env);
	}
}
