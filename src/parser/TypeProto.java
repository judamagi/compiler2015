package parser;

public class TypeProto {
	public TypeProto(String string) {
		type = string;
	}
	public TypeProto(String string, RecordDecl tree) {
		type = string;
		decl = tree;
	}
	public String type;
	public RecordDecl decl=null;
	public boolean isBase() {
		return type.equals("int") || type.equals("void") || type.equals("char");
	}
}
