package parser;

import Env.Env;
import IR.IntegerConstant;
import error.SemanticError;

public class IntegerLiteral extends Literal {
	public IntegerLiteral(int numVal) {
		integerLiteral = numVal;
	}

	int integerLiteral;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<IntegerLiteral :%d >\n",blank,integerLiteral);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		return new annotated(Parser.INT, false,new IntegerConstant(integerLiteral));
	}
	@Override
	public Object getValue() {
		return integerLiteral;
	}
}
