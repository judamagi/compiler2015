package parser;

import java.util.ArrayList;

import Env.Env;
import error.SemanticError;

public class TranslationUnitDecl extends Decl {
	ArrayList<Decl> declList = new ArrayList<Decl>();
	public void addDecl(Decl decl) {
		declList.add(decl);
	}
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<TranslationUnitDecl>\n",blank);
		for (Decl decl : declList) {
			decl.show(indent+1);
		}
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		Env globalEnv = new Env();
		for (Decl decl : declList) {
			decl.check(globalEnv);
		}
		return null;
	}
}
