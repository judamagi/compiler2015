package parser;

import Env.Env;
import IR.CharConstant;
import IR.Instruction;
import IR.Temp;
import IR.__getchar__;
import error.SemanticError;

public class Getchar extends Stmt {

	@Override
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<getchar>",blank);
		
	}

	@Override
	public annotated check(Env env) throws SemanticError {
		Temp temp = new Temp();
		Instruction.add(new __getchar__(temp));		
		return new annotated(Parser.CHAR, false,temp);
	}

}
