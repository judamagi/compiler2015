package parser;

import error.SemanticError;
import Env.Env;
import IR.IntegerConstant;
import IR.Name;


public class Pointer extends Type {
	public Pointer(String type) {
		originType = type;
	}

	public Pointer(Type baseType) {
		elemType = baseType;
	}

	public String toString() {
		if(originType != null) return originType; else return elemType.toString()+"*";
	};
	
	public String originType=null;
	public Type elemType=null;
	@Override
	public boolean typeEquals(Type baseType) {
		if(!(baseType instanceof Pointer))return false;
		Pointer temp = (Pointer) baseType;
		if(originType == null) {
			return temp.elemType!=null && elemType.typeEquals(temp.elemType);
		} else {
			return temp.originType!=null && originType.equals(temp.originType);
		}
	}

	public Type getRealType(Env env) throws SemanticError {
		if(originType != null) {
			if(originType.equals("int")) return Parser.INT;
			if(originType.equals("char")) return Parser.CHAR;
			if(originType.equals("void")) return Parser.VOID;
			if(env.containsKeyStructure(originType)) return env.getKeyStructure(originType); else throw new SemanticError("undefined type:"+originType);
			
		} else return elemType;
	}

	@Override
	int calcSize(Env env) throws SemanticError {
		return 4;
	}

	@Override
	public
	int getConstSpace() throws SemanticError {
		return 4;
	}

	@Override
	public int getRealSize(Env env) throws SemanticError {
		return 4;
	}

}
