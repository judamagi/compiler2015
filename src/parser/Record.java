package parser;

import java.util.ArrayList;
import java.util.Map;

import error.SemanticError;
import Env.Env;
import IR.Name;

public abstract class Record extends Type {
	Map<String, Type> elements;
	String name;
	abstract Name calcOffset(String identifier, Env env) throws SemanticError;
	/*public boolean hasDecl(){
		return tree!=null;
	}
	public RecordDecl getDecl(){
		return tree;
	}
	ArrayList<Decl> fields = new ArrayList<Decl>();
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<Record>\n",blank);
		for (Decl recordField : fields) {
			recordField.show(indent+1);
		}
	}*/
}
