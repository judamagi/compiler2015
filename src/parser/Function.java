package parser;

import java.util.ArrayList;

import error.SemanticError;
import Env.Env;
import IR.IntegerConstant;
import IR.Name;

public class Function extends Type {
	public Function(Type returnType2, ArrayList<Type> temp) {
		returnType = returnType2;
		paramType = temp;
	}
	public Type returnType;
	public ArrayList<Type> paramType;
	@Override
	public boolean typeEquals(Type baseType) {
		//
		return true;
	}
	@Override
	int calcSize(Env env) {
		return 4;
	}
	@Override
	public
	int getConstSpace() {
		return 4;
	}
	@Override
	public int getRealSize(Env env) throws SemanticError {
		return 4;
	}
}
