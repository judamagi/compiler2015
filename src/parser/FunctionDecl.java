package parser;

import java.util.ArrayList;

import Env.Env;
import IR.FunctionReturn;
import IR.FuntionStart;
import IR.Instruction;
import IR.Label;
import IR.LocalVariableRequest;
import error.SemanticError;

public class FunctionDecl extends Decl {
	Type returnType;
	IdentifierExpr functionName;
	ArrayList<ParamVarDecl> paramList;
	CompoundStmt compoundStmt;
	String type;
	public FunctionDecl(String type, IdentifierExpr declarator,
			ArrayList<ParamVarDecl> paramList2, CompoundStmt CompoundStmt) {
		this.type = type;
		functionName = declarator;
		this.compoundStmt = CompoundStmt;
		paramList = paramList2;
	}
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<FunctionDecl: Type: %s>\n",blank,returnType.toString());
		functionName.show(indent+1);
		for (ParamVarDecl paramVarDecl : paramList) {
			paramVarDecl.show(indent+1);
		}
		compoundStmt.show(indent+1);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		Type baseType;
		String temp = new String(functionName.identifier);
		if(temp.startsWith("*")){
			baseType = new Pointer(type);
			temp=temp.substring(1);
			while(temp.startsWith("*")){
				baseType = new Pointer(baseType);
				temp = temp.substring(1);
			}
		} else {		
			if(type.equals("int")) baseType = Parser.INT; else
			if(type.equals("void")) baseType = Parser.VOID; else
			if(type.equals("char")) baseType = Parser.CHAR; else {
				if(!env.containsKeyStructure(type))throw new SemanticError("undefined record:"+type);
				baseType = env.getKeyStructure(type);
			}
		}
		returnType = baseType;
		
		if(env.tables.containsKey(temp)) throw new SemanticError("redefined function:"+temp);
		ArrayList<Type> tt = new ArrayList<>();
		Instruction.add(new FuntionStart(temp,paramList.size()));	
		Label l = new Label();
		Env subEnv = new Env(env,true,true);
		subEnv.FunctionReturnLabel = l;
		subEnv.functionName = temp;
		subEnv.changeGlobal();
		for (ParamVarDecl param : paramList) {
			annotated res = param.check(subEnv);
			tt.add(res.type);
		}
		env.tables.put(temp, new Function(returnType, tt));			
		compoundStmt.check(subEnv);
		Instruction.add(l);
		Instruction.add(new FunctionReturn(temp.equals("main")));
		return null;
	}
	
}
