package parser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Env.Env;
import IR.Instruction;
import IR.Name;
import IR.Variable;
import IR.getAddress;
import error.SemanticError;

public class DeclRefExpr extends Stmt {
	public DeclRefExpr(String identifier) {
		this.valName = identifier;
	}

	String valName;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<DeclRefExpr: %s>\n",blank,valName);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		if(!(env.containsKeyTables(valName)))throw new SemanticError(valName+" must decleared first");
		Type type = env.getKeyTables(valName);
		String name;
		if(env.isKeyBelongGlobal(valName))name = new String(valName); 
			else name = new String(valName+"#"+env.functionName+String.valueOf(env.getLevelByKeyTables(valName)));
		if(type instanceof Array) return new annotated(type, false,getRefName(name,type));
			else return new annotated(type, true,getRefName(name,type));
	}
	
	
	private Name getRefName(String name,Type type) {
		if(Env.isUsed.containsKey(name))return Env.isUsed.get(name);
		Variable temp = new Variable(name,type instanceof Record,type instanceof Array);
		Env.isUsed.put(name, temp);
		return temp;
	}
}
