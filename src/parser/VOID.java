package parser;

import error.SemanticError;
import Env.Env;
import IR.IntegerConstant;
import IR.Name;

final public class VOID extends Type {
	@Override
	public String toString() {
		return "void";
	}

	@Override
	public boolean typeEquals(Type baseType) {
		return baseType instanceof VOID;
	}

	@Override
	int calcSize(Env env) {
		return 4;
	}

	@Override
	public
	int getConstSpace() {
		return 4;
	}

	@Override
	public int getRealSize(Env env) throws SemanticError {
		return 1;
	}
	
}
