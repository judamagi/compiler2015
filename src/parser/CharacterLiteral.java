package parser;

import java.util.List;

import Env.Env;
import IR.CharConstant;
import IR.Instruction;
import IR.Name;
import error.SemanticError;

public class CharacterLiteral extends Literal {
	public CharacterLiteral(char charVal) {
		// TODO Auto-generated constructor stub
		characterLiteral = charVal;
	}

	public char characterLiteral;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<CharacterLiteral: %c >\n",blank,characterLiteral);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		return new annotated(Parser.CHAR, false, new CharConstant(characterLiteral));
	}
	@Override
	public Object getValue() {
		return characterLiteral;
	}
}