package parser;

import java.util.Random;

import Env.Env;
import IR.StringConstant;
import error.SemanticError;

public abstract class Stmt extends AbstractSyntaxTree {
	static public int calcConstantExpression(Stmt expr,Env env) throws SemanticError {
		if (expr instanceof CharacterLiteral) return (int)(((CharacterLiteral)expr).characterLiteral);
		if (expr instanceof IntegerLiteral) return ((IntegerLiteral) expr).integerLiteral;
		//if (expr instanceof StringLiteral) return StringConstant.getStringAddress(((StringLiteral) expr).stringLiteral);
		if (expr instanceof UnaryOperator) {
			if(((UnaryOperator) expr).Op.equals("-")) return -calcConstantExpression(((UnaryOperator) expr).statement,env);
			if(((UnaryOperator) expr).Op.equals("+")) return calcConstantExpression(((UnaryOperator) expr).statement,env);
			if(((UnaryOperator) expr).Op.equals("!")) return transBoolToInt(!transIntToBool(calcConstantExpression(((UnaryOperator) expr).statement,env)));
			if(((UnaryOperator) expr).Op.equals("~")) return ~calcConstantExpression(((UnaryOperator) expr).statement,env);
			if(((UnaryOperator) expr).Op.equals("&")) {
				((UnaryOperator) expr).statement.check(env);
				return 1000000+(int)(Math.random()*100000);
			}
		}
		if (expr instanceof SizeofOperator) return ((SizeofOperator) expr).getConstSize(env);
		if (expr instanceof BinaryOperator) {
			if(((BinaryOperator) expr).Op.equals("*")) 
				return calcConstantExpression(((BinaryOperator) expr).lhs,env) * calcConstantExpression(((BinaryOperator) expr).rhs,env);
			if(((BinaryOperator) expr).Op.equals("/")) 
				return calcConstantExpression(((BinaryOperator) expr).lhs,env) / calcConstantExpression(((BinaryOperator) expr).rhs,env);
			if(((BinaryOperator) expr).Op.equals("%")) 
				return calcConstantExpression(((BinaryOperator) expr).lhs,env) % calcConstantExpression(((BinaryOperator) expr).rhs,env);
			if(((BinaryOperator) expr).Op.equals("+")) 
				return calcConstantExpression(((BinaryOperator) expr).lhs,env) + calcConstantExpression(((BinaryOperator) expr).rhs,env);
			if(((BinaryOperator) expr).Op.equals("-")) 
				return calcConstantExpression(((BinaryOperator) expr).lhs,env) - calcConstantExpression(((BinaryOperator) expr).rhs,env);
			if(((BinaryOperator) expr).Op.equals(">>")) 
				return calcConstantExpression(((BinaryOperator) expr).lhs,env) >> calcConstantExpression(((BinaryOperator) expr).rhs,env);
			if(((BinaryOperator) expr).Op.equals("<<")) 
				return calcConstantExpression(((BinaryOperator) expr).lhs,env) << calcConstantExpression(((BinaryOperator) expr).rhs,env);
			if(((BinaryOperator) expr).Op.equals(">")) 
				return transBoolToInt(calcConstantExpression(((BinaryOperator) expr).lhs,env) > calcConstantExpression(((BinaryOperator) expr).rhs,env));
			if(((BinaryOperator) expr).Op.equals("<")) 
				return transBoolToInt(calcConstantExpression(((BinaryOperator) expr).lhs,env) < calcConstantExpression(((BinaryOperator) expr).rhs,env));
			if(((BinaryOperator) expr).Op.equals(">=")) 
				return transBoolToInt(calcConstantExpression(((BinaryOperator) expr).lhs,env) >= calcConstantExpression(((BinaryOperator) expr).rhs,env));
			if(((BinaryOperator) expr).Op.equals("<=")) 
				return transBoolToInt(calcConstantExpression(((BinaryOperator) expr).lhs,env) <= calcConstantExpression(((BinaryOperator) expr).rhs,env));
			if(((BinaryOperator) expr).Op.equals("==")) 
				return transBoolToInt(calcConstantExpression(((BinaryOperator) expr).lhs,env) == calcConstantExpression(((BinaryOperator) expr).rhs,env));
			if(((BinaryOperator) expr).Op.equals("!=")) 
				return transBoolToInt(calcConstantExpression(((BinaryOperator) expr).lhs,env) != calcConstantExpression(((BinaryOperator) expr).rhs,env));
			if(((BinaryOperator) expr).Op.equals("&")) 
				return calcConstantExpression(((BinaryOperator) expr).lhs,env) & calcConstantExpression(((BinaryOperator) expr).rhs,env);
			if(((BinaryOperator) expr).Op.equals("|")) 
				return calcConstantExpression(((BinaryOperator) expr).lhs,env) | calcConstantExpression(((BinaryOperator) expr).rhs,env);
			if(((BinaryOperator) expr).Op.equals("^")) 
				return calcConstantExpression(((BinaryOperator) expr).lhs,env) ^ calcConstantExpression(((BinaryOperator) expr).rhs,env);
			if(((BinaryOperator) expr).Op.equals("&&")) 
				if(calcConstantExpression(((BinaryOperator) expr).lhs,env)==0) return 0; else return (calcConstantExpression(((BinaryOperator) expr).rhs,env));
			if(((BinaryOperator) expr).Op.equals("||")) 
				if(calcConstantExpression(((BinaryOperator) expr).lhs,env)!=0) return 1; else return (calcConstantExpression(((BinaryOperator) expr).rhs,env));
			
		}
		throw new SemanticError("no a constant expression");
	}

	static private boolean transIntToBool(int x) {
		return x==0?false:true;
	}

	private static int transBoolToInt(boolean b) {
		return b?1:0;
	}
}
