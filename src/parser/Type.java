package parser;

import Env.Env;
import IR.Name;
import error.SemanticError;

abstract public class Type {
	
	/*
	 * binary operator
	 */
	static public Type matchBinary(Type t1, Type t2) throws SemanticError {
		if(t1 == null || t2 == null) throw new SemanticError("can compare a type with null");
		if(t1 instanceof INT && t2 instanceof INT) return t1;
		if(t1 instanceof CHAR && t2 instanceof CHAR) return t1;
		if(t1 instanceof CHAR && t2 instanceof INT) return t2;
		if(t1 instanceof INT && t2 instanceof CHAR) return t1;
		if(t1 instanceof INT && t2 instanceof Pointer) return t2;
		if(t1 instanceof Pointer && t2 instanceof INT) return t1;
		if(t1 instanceof CHAR && t2 instanceof Pointer) return t2;
		if(t1 instanceof Pointer && t2 instanceof CHAR) return t1;
		throw new SemanticError(String.format("unmatch type: %s and %s",t1.toString(),t2.toString()));
	}
	
	
	/*
	 * assign operator
	 */
	static public Type matchAssign(Type t1, Type t2) throws SemanticError {
		if(t1 == null || t2 == null) throw new SemanticError("can compare a type with null");
		if(t1 instanceof INT && t2 instanceof INT) return t1;
		if(t1 instanceof CHAR && t2 instanceof CHAR) return t1;
		if(t1 instanceof CHAR && t2 instanceof INT) return t1;
		if(t1 instanceof INT && t2 instanceof CHAR) return t1;
		if(t1 instanceof INT && t2 instanceof Pointer) return t1;
		if(t1 instanceof Pointer && t2 instanceof INT) return t2;
		if(t1 instanceof CHAR && t2 instanceof Pointer) return Parser.INT;
		if(t1 instanceof Pointer && t2 instanceof CHAR) return Parser.INT;
		if(t1 instanceof Pointer && (t2 instanceof Pointer || t2 instanceof INT || t2 instanceof CHAR)) return t1;
		if(t1 instanceof Record && t2 instanceof Record && ((Record) t1).name.equals(((Record) t2).name)) return t1;
		throw new SemanticError(String.format("unmatch type: %s and %s",t1.toString(),t2.toString()));
	}


	abstract public boolean typeEquals(Type baseType) ;
	abstract int calcSize(Env env) throws SemanticError;
	public abstract int getConstSpace() throws SemanticError;


	public abstract int getRealSize(Env env) throws SemanticError ;
	
}
