package parser;

import java.util.List;

import Env.Env;
import IR.Instruction;
import IR.Name;
import error.SemanticError;

public class DoNoThing extends Stmt {

	@Override
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<do nothing>\n",blank);
	}

	@Override
	public annotated check(Env env) throws SemanticError {		
		return null;
	}

}
