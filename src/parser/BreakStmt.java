package parser;

import java.util.List;

import Env.Env;
import IR.Goto;
import IR.Instruction;
import IR.Name;
import error.SemanticError;

public class BreakStmt extends Stmt {
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<BreakStmt>\n",blank);
	}

	@Override
	public annotated check(Env env) throws SemanticError {
		// TODO Auto-generated method stub
		if(env.Loop == 0) throw new SemanticError("break isn't in a loop");
		Instruction.add(new Goto(env.SkipBlockLabel));
		return null;
	}
}
