package parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Env.Env;
import IR.Assign;
import IR.BinaryOperaterAssgin;
import IR.IfFalseGoto;
import IR.IfGoto;
import IR.Instruction;
import IR.IntegerConstant;
import IR.Label;
import IR.Name;
import IR.Temp;
import IR.Variable;
import error.SemanticError;

public class BinaryOperator extends Stmt {
	public BinaryOperator(String binOp, Stmt lHS2, Stmt rHS2) {
		// TODO Auto-generated constructor stub
		Op = new String(binOp);
		lhs = lHS2;
		rhs = rHS2;
	}
	String Op;
	Stmt lhs,rhs;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<BinaryOperator: Opeartor %s >\n",blank,Op);
		lhs.show(indent+1);
		rhs.show(indent+1);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		if(Op.equals("&&")) {
			annotated t1,t2;
			t1 = lhs.check(env);
			Label l1 = new Label();
			
			Variable tt = new Temp();
			Instruction.add(new Assign(tt, t1.name,4));
			Instruction.add(new IfFalseGoto(t1.name, l1));
			t2 = rhs.check(env);
			Instruction.add(new Assign(tt, t2.name,4));
			Instruction.add(l1);
			Type t3 = Type.matchBinary(t1.type, t2.type);
			return new annotated(t3, false,tt);
		}
		if(Op.equals("||")) {
			annotated t1,t2;
			t1 = lhs.check(env);			
			Label l1 = new Label();
			Variable tt = new Temp();
			Instruction.add(new Assign(tt, t1.name,4));
			Instruction.add(new IfGoto(t1.name, l1));
			t2 = rhs.check(env);
			Instruction.add(new Assign(tt, t2.name,4));
			Instruction.add(l1);
			Type t3 = Type.matchBinary(t1.type, t2.type);
			return new annotated(t3, false,tt);
		}
		annotated t1 = lhs.check(env), t2 = rhs.check(env);
		if(isCompare(Op)) {			
			Type t3 = Type.matchAssign(t1.type, t2.type);
			Variable tt = new Temp();		
			if(t1.type instanceof Record || t2.type instanceof Record) throw new SemanticError("can't compare "+t1.type.toString()+" and "+t2.type.toString());
			Instruction.add(new BinaryOperaterAssgin(tt, t1.name, t2.name, Op));
			return new annotated(Parser.INT, false,tt);
		}		
		if(Op.equals(","))return new annotated(t2.type, false, t2.name);
		Type t3 = Type.matchBinary(t1.type, t2.type);
		Variable tt = new Temp();			
		if(Op.equals("-") && t1.type instanceof Pointer && t2.type instanceof Pointer) {
			if(!t1.type.typeEquals(t2.type))throw new SemanticError("can't minus between different type pointer");
			Instruction.add(new BinaryOperaterAssgin(tt, t1.name, t2.name, Op));
			return new annotated(Parser.INT, false,tt);
		}
		
		if((Op.equals("+") || Op.equals("-")) && (t1.type instanceof Pointer || t2.type instanceof Pointer)){
			Variable g2 = new Temp();
			tt = new Temp();
			if(t1.type instanceof Pointer) {
				Name g1 = new IntegerConstant(((Pointer)(t1.type)).getRealType(env).getConstSpace());
				Instruction.add(new BinaryOperaterAssgin(g2, g1, t2.name, "*"));
				Instruction.add(new BinaryOperaterAssgin(tt, t1.name, g2, Op));
			}
			if(t2.type instanceof Pointer) {
				Name g1 = new IntegerConstant(((Pointer)(t2.type)).getRealType(env).getConstSpace());
				Instruction.add(new BinaryOperaterAssgin(g2, g1, t1.name, "*"));
				Instruction.add(new BinaryOperaterAssgin(tt, g2, t2.name, Op));
			}			
			return new annotated(t3, false,tt);
		}
		Instruction.add(new BinaryOperaterAssgin(tt, t1.name, t2.name, Op));
		return new annotated(t3, false,tt);
	}
	private boolean isCompare(String op) {
		final String[] compareOp={"==","!=","<",">","<=",">="};
		return Arrays.asList(compareOp).contains(op.intern());
	}
}
