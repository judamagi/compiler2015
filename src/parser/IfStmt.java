package parser;

import Env.Env;
import IR.Goto;
import IR.IfFalseGoto;
import IR.Instruction;
import IR.Label;
import error.SemanticError;

public class IfStmt extends Stmt {
	public IfStmt(Stmt expr1, Stmt expr2, Stmt expr3) {
		statment = expr1;
		statment1 = expr2;
		statment2 = expr3;
	}
	public IfStmt(Stmt expr1, Stmt expr2) {
		statment = expr1;
		statment1 = expr2;
		statment2 = null;
	}
	Stmt statment;
	Stmt statment1;
	Stmt statment2;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<IfStmt>\n",blank);
		statment.show(indent+1);
		statment1.show(indent+1);
		if(statment2!=null)statment2.show(indent+1);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		annotated res = statment.check(env);
		Label l1 = new Label(), l2 = new Label();
		Instruction.add(new IfFalseGoto(res.name, l1));
		try {
			Type.matchAssign(Parser.INT, res.type);
		} catch (SemanticError e) {
			throw new SemanticError("wrong type of condition of if statement ");
		}
		statment1.check(env);
		Instruction.add(new Goto(l2));
		Instruction.add(l1);
		if(statment2 != null) statment2.check(env);
		Instruction.add(l2);
		return null;
	}
}
