package parser;

import Env.Env;
import IR.Goto;
import IR.Instruction;
import IR.Name;
import IR.SetReturnValue;
import IR.Temp;
import IR.solvePointer;
import error.SemanticError;

public class ReturnStmt extends Stmt {
	public ReturnStmt(Stmt expr) {
		statment = expr;
	}

	Stmt statment = null;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<ReturnStmt>\n",blank);
		statment.show(indent+1);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		if(!(statment instanceof DoNoThing)){
			annotated s1;
			if((s1=statment.check(env))==null) throw new SemanticError("must have return value");
			Name t1;
			if(s1.type instanceof Record && !(s1.name instanceof Temp)) {
				t1 = new Temp();
				Instruction.add(new solvePointer(t1, s1.name, s1.type.calcSize(env)));
			} else {
				t1 = s1.name;
			}
			Instruction.add(new SetReturnValue(t1,s1.type.calcSize(env)));
		}
		Instruction.add(new Goto(env.FunctionReturnLabel));
		return null;
	}
}
