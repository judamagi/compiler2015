package parser;

import java.util.ArrayList;

import Env.Env;
import IR.BinaryOperaterAssgin;
import IR.Instruction;
import IR.IntegerConstant;
import IR.Name;
import IR.Temp;
import IR.Variable;
import error.SemanticError;

public class Array extends Pointer {

	
	public Array(Type ty, Stmt expression) throws SemanticError {
		super(ty);
		capacity = expression;
	}

	int size = -1;
	@SuppressWarnings("static-access")
	public int calcSize(Env env) throws SemanticError {
		if(size != -1) return size;
		size = capacity.calcConstantExpression(capacity, env);
		Type temp = this.getRealType(env);
		if(temp instanceof CHAR)size = (size/4+1)*4;else size = size * temp.calcSize(env);
		return size;
	}

	Stmt capacity; 
	public int getConstSpace() throws SemanticError {
		return size;
	}
	
	@Override
	public int getRealSize(Env env) throws SemanticError {
		return capacity.calcConstantExpression(capacity, env)*elemType.getRealSize(env);
	}
}
