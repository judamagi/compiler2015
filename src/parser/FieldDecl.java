package parser;

import Env.Env;
import error.SemanticError;

public class FieldDecl extends Decl {
	public IdentifierExpr fieldName;
	public String type;
	public FieldDecl(String type2, IdentifierExpr id) {
		this.type = type2;
		this.fieldName = id;
	}
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<FieldDecl: fieldName: %s fieldType: %s>\n",blank,fieldName,type.toString());
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		Type baseType;
		String temp = new String(fieldName.identifier);
		if(temp.startsWith("*")){
			baseType = new Pointer(type);
			temp=temp.substring(1);
			while(temp.startsWith("*")){
				baseType = new Pointer(baseType);
				temp=temp.substring(1);
			}
		} else {		
			if(type.equals("int")) baseType = Parser.INT; else
			if(type.equals("void")) throw new SemanticError("void type rejected"); else
			if(type.equals("char")) baseType = Parser.CHAR; else {
				if(!env.containsKeyStructure(type))throw new SemanticError("undefined record");
				baseType = env.getKeyStructure(type);
			}
		}
		if(fieldName instanceof IdentifierArray){
			for (Stmt stmt : ((IdentifierArray) fieldName).expression) {
				if(env.isGlobal()) {					
					int x = Stmt.calcConstantExpression(stmt, env);
					baseType = new Array(baseType, new IntegerLiteral(x));
				} else {
					try {
						int x = Stmt.calcConstantExpression(stmt, env);
						if(x<0) throw new SemanticError("cant' negative array size"); else baseType = new Array(baseType, new IntegerLiteral(x));
					} catch (Exception e) {
						if(e.getMessage() == "cant' negative array size") throw e;
						stmt.check(env);
						baseType = new Array(baseType, stmt);						
					}
				}
			}
		}
		baseType.calcSize(env);
		if(env.tables.containsKey(temp)) throw new SemanticError(temp+" redefined");
		env.tables.put(temp, baseType);
		return null;
	}
}
