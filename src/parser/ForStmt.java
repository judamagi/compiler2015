package parser;

import Env.Env;
import IR.Goto;
import IR.IfFalseGoto;
import IR.Instruction;
import IR.Label;
import error.SemanticError;

public class ForStmt extends Stmt {
	public ForStmt(Stmt expr1, Stmt expr2, Stmt expr3, Stmt expr4) {
		initial = expr1;
		terminate = expr2;
		steps = expr3;
		statement = expr4;
	}

	Stmt initial,terminate,steps,statement;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<ForStmt>\n",blank);
		if(initial==null)System.err.printf("%s\t<<NULL>>\n",blank);else initial.show(indent+1);
		if(terminate==null)System.err.printf("%s\t<<NULL>>\n",blank);else terminate.show(indent+1);
		if(steps==null)System.err.printf("%s\t<<NULL>>\n",blank);else steps.show(indent+1);
		statement.show(indent+1);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		annotated s2;		
		Label l1 = new Label(),l2 = new Label(),l3 = new Label();
		Env subenv = new Env(env,true,true);
		subenv.StartBlockLabel = l2;
		subenv.SkipBlockLabel = l3;
		if(initial != null && (initial.check(env))==null) throw new SemanticError("initial statement must expression");
		Instruction.add(l1);
		if(terminate != null) {
			if((s2=terminate.check(env))==null) throw new SemanticError("terminate statement must expression");
			Instruction.add(new IfFalseGoto(s2.name, l3));
		}				
		subenv.Loop += 1;
		statement.check(subenv);
		subenv.Loop -= 1;
		Instruction.add(l2);
		if(steps != null && (steps.check(env))==null) throw new SemanticError("steps statement must expression");
		Instruction.add(new Goto(l1));
		Instruction.add(l3);
		return null;
	}
}
