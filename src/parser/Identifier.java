package parser;

import Env.Env;
import IR.Variable;
import error.SemanticError;

public class Identifier extends IdentifierExpr {

	public Identifier(String identifierStr) {
		super(identifierStr);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<Identifier: %s>\n",blank,identifier);
	}

	@Override
	public annotated check(Env env) throws SemanticError {
		if(!(env.containsKeyTables(identifier)))throw new SemanticError("undefined variable:"+identifier);
		return new annotated(env.getKeyTables(identifier), true,null);
	}
}
