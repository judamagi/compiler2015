package parser;

import java.util.List;

import Env.Env;
import IR.Goto;
import IR.Instruction;
import IR.Name;
import error.SemanticError;

public class ContinueStmt extends Stmt {
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<ContinueStmt>\n",blank);
	}

	@Override
	public annotated check(Env env) throws SemanticError {
		if(env.Loop == 0 )throw new SemanticError("continue must in a loop");
		Instruction.add(new Goto(env.StartBlockLabel));
		return null;
	}
}
