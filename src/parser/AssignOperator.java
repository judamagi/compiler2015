package parser;



import java.util.List;

import error.SemanticError;
import Env.Env;
import IR.Assign;
import IR.BinaryOperaterAssgin;
import IR.Constant;
import IR.Instruction;
import IR.Name;
import IR.PointerWrite;
import IR.Temp;
import IR.Variable;
import IR.solvePointer;

public class AssignOperator extends Stmt {
	public AssignOperator(Stmt lhs2, String opStr, Stmt parseAssignExprssion) {
		Op = opStr;
		lhs = lhs2;
		rhs = parseAssignExprssion;
	}
	public String Op;
	public Stmt lhs,rhs;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<AssignOperator: Opeartor %s >\n",blank,Op);
		lhs.show(indent+1);
		rhs.show(indent+1);
	}
	public annotated check(Env env) throws SemanticError {
		annotated t1 = lhs.check(env);
		Instruction temp = Instruction.getLastInstruction();		
		int u = -1;
		if(temp instanceof solvePointer && temp.x() == t1.name) {	
			Name x,y;
			y = temp.x();
			x = temp.y();
			if(Op.equals("=")) {
				Instruction.removeLastInstruction();
				annotated t2 = rhs.check(env);
				Type t3 = Type.matchAssign(t1.type, t2.type);
				if(!t1.isLeftValue) throw new SemanticError("isn't left value");
				Name tq;
				if(t1.type instanceof Record && t2.name instanceof Variable && !(t2.name instanceof Temp)) {
					tq = new Temp();
					Instruction.add(new solvePointer(tq, t2.name, t2.type.calcSize(env)));					
				} else {
					tq = t2.name;
				}
				Instruction.add(new PointerWrite(x, tq,t1.type.calcSize(env)));
				return new annotated(t3, true, t2.name);
			} else {
				annotated t2 = rhs.check(env);
				Type t3 = Type.matchAssign(t1.type, t2.type);
				if(!t1.isLeftValue) throw new SemanticError("isn't left value");
				String operator;
				if(Op.length()==2)operator = Op.substring(0, 1); else operator = Op.substring(0, 2);
				Variable s = new Temp();
				Instruction.add(new BinaryOperaterAssgin(s, y, t2.name, operator));
				Instruction.add(new  PointerWrite(x, s,t1.type.calcSize(env)));
				return new annotated(t3, true, s);
			}			
		} else {
			annotated t2 = rhs.check(env);
			Type t3 = Type.matchAssign(t1.type, t2.type);
			if(!t1.isLeftValue) throw new SemanticError("isn't left value");
			Variable s1=(Variable) t1.name;
			Name s2=t2.name;
			if(Op.equals("=")){
				if(t1.type instanceof Record) {
					Name tt, ts = new Temp();
					if(t1.type instanceof Record && t2.name instanceof Variable && !(t2.name instanceof Temp)) {
						tt = new Temp();
						Instruction.add(new solvePointer(tt, s2, t2.type.calcSize(env)));						
					} else {
						tt = t2.name;
					}
					Instruction.add(new PointerWrite(s1, tt, t2.type.calcSize(env)));
					Instruction.add(new solvePointer(ts, s1, t2.type.calcSize(env)));
					return new annotated(t3, true, ts);					
				} else {
					int cc;
					if(t1.type instanceof CHAR)cc=4;else cc = t1.type.calcSize(env); 
					Instruction.add(new Assign(s1,s2,cc));
				}
			} else {
				Variable tt = new Temp();
				Instruction.add(new Assign(tt, s1,t1.type.calcSize(env)));
				String operator;
				if(Op.length()==2)operator = Op.substring(0, 1); else operator = Op.substring(0, 2);
				Instruction.add(new BinaryOperaterAssgin(s1,tt,s2,operator));
			}
			return new annotated(t3, true,s1);
		}
	}	
}
