package parser;

import Env.Env;
import IR.Assign;
import IR.BinaryOperaterAssgin;
import IR.Instruction;
import IR.IntegerConstant;
import IR.Name;
import IR.Temp;
import IR.Variable;
import IR.getAddress;
import IR.solvePointer;
import error.SemanticError;

public class ArrayAddressOperator extends Stmt {

	Stmt expr;
	Stmt owner;
	
	public ArrayAddressOperator(Stmt owner, Stmt expr) {
		this.owner = owner;
		this.expr = expr;
	}

	@Override
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<ArrayAddressOperator: [] >\n",blank);
		owner.show(indent+1);
		expr.show(indent+1);
	}

	@Override
	public annotated check(Env env) throws SemanticError {
		annotated res = owner.check(env), res2;
		if(!(res.type instanceof Array)){ // int ** etc.
			res2 = expr.check(env);
			if(!(res.type instanceof Pointer))throw new SemanticError("[] owner no a pointer type");
			Type.matchAssign(Parser.INT,res2.type);
			Type temp = ((Pointer)res.type).getRealType(env);
			Variable t1 = new Temp(), t2 = new Temp(), t3 = new Temp();
			Instruction.add(new BinaryOperaterAssgin(t1,res2.name,new IntegerConstant(temp.calcSize(env)),"*"));
			Instruction.add(new BinaryOperaterAssgin(t2, res.name, t1, "+"));
			Instruction.add(new solvePointer(t3, t2,temp.calcSize(env)));
			return new annotated(temp, true, t3);
		}
		Instruction lastInstruction = Instruction.getLastInstruction();
		Name s4;
		if(lastInstruction instanceof solvePointer && lastInstruction.x() == res.name) {
			s4 = lastInstruction.y();
			Instruction.removeLastInstruction();
		} else {
			s4 = res.name;
		}
		res2 = expr.check(env);
		if(!(res.type instanceof Pointer))throw new SemanticError("[] owner no a array type");
		Type.matchAssign(Parser.INT,res2.type);
		Type temp = ((Pointer)res.type).getRealType(env);
		Name s1 = new IntegerConstant(temp.getConstSpace());
		Variable s2 = new Temp(),s5=new Temp(), s3 = new Temp();
		Instruction.add(new BinaryOperaterAssgin(s2, res2.name, s1, "*"));
		Instruction.add(new BinaryOperaterAssgin(s3, s4, s2, "+"));		
		if(temp instanceof Array) {			
			return new annotated(temp, false,s3); 
		}else {
			Instruction.add(new solvePointer(s5, s3,temp.calcSize(env)));
			return new annotated(temp, true,s5);			
		}
	}

}
