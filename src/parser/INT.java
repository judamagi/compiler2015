package parser;

import Env.Env;
import IR.IntegerConstant;
import IR.Name;

final public class INT extends Type {
	@Override
	public String toString() {
		return "int";
	}

	@Override
	public boolean typeEquals(Type baseType) {
		return baseType instanceof INT;
	}

	@Override
	int calcSize(Env env) {
		return 4;
	}

	@Override
	public
	int getConstSpace() {
		return 4;
	}

	@Override
	public int getRealSize(Env env) {
		return 4;
	}
}
