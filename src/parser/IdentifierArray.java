package parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import Env.Env;
import error.SemanticError;

public class IdentifierArray extends IdentifierExpr {
	public IdentifierArray(String identifierStr) {
		super(identifierStr);
	}
	
	@SuppressWarnings("unchecked")
	public void add(Stmt expr) throws SemanticError {
		expression.add(0,expr);
		
	}

	public List<Stmt> expression = new LinkedList<Stmt>();
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<IdentifierArray: %s>\n",blank,identifier);
		for (Stmt stmt : expression) {
			stmt.show(indent+1);
		}
	}

	@Override
	public annotated check(Env env) throws SemanticError {
		if(!(env.containsKeyTables(identifier)))throw new SemanticError("undefined array");
		if(!(env.getKeyTables(identifier) instanceof Array)) throw new SemanticError(identifier+" isn't a array");
		return null;
	}
}
