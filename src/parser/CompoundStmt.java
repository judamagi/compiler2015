package parser;

import java.util.ArrayList;
import java.util.List;

import Env.Env;
import IR.Instruction;
import IR.Name;
import error.SemanticError;

public class CompoundStmt extends Stmt {
	ArrayList<Stmt> compoundStmtList = new ArrayList<Stmt>();
	public void add(Stmt stmt) {
		compoundStmtList.add(stmt);
	}
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<CompoundStmt>\n",blank);
		for (Stmt stmt : compoundStmtList) {
			stmt.show(indent+1);
		}
	}
	
	@Override
	public annotated check(Env env) throws SemanticError {
		Env subenv = new Env(env,true,true);
		for (Stmt stmt : compoundStmtList) {
			stmt.check(subenv);		
		}
		return new annotated(null,false,null);
	}
}
