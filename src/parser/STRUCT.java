package parser;

import java.util.Map.Entry;

import error.SemanticError;
import Env.Env;
import IR.BinaryOperaterAssgin;
import IR.Instruction;
import IR.IntegerConstant;
import IR.Name;
import IR.Temp;
import IR.Variable;

public final class STRUCT extends Record {
	public STRUCT(String name,Env env) {
		this.name = name;
		elements = env.tables;
	}
	@Override
	public String toString() {
		return name;
	}
	@Override
	Name calcOffset(String identifier,Env env) throws SemanticError {
		int s = 0;
		for ( Entry<String, Type> iter : elements.entrySet()) {
			if(iter.getKey().endsWith(identifier))break;
			s += Math.max(4,iter.getValue().calcSize(env));
		}
		return new IntegerConstant(s);
	}
	@Override
	public boolean typeEquals(Type baseType) {
		if(baseType instanceof STRUCT){
			return baseType == this;
		}
		return false;
	}
	
	int __size = -1;
	@Override	
	int calcSize(Env env) throws SemanticError {
		if(__size != -1) return __size;
		int res = 0,s;
		for ( Entry<String, Type> iter : elements.entrySet()) {
			s = Math.max(4,iter.getValue().calcSize(env));
			res += s;
		}
		__size = res;
		return res;
	}
	@Override
	public
	int getConstSpace() throws SemanticError {
		int res = 0,s;
		for ( Entry<String, Type> iter : elements.entrySet()) {
			s = Math.max(4,iter.getValue().getConstSpace());
			res += s;
		}
		return res;
	}
	@Override
	public int getRealSize(Env env) throws SemanticError {
		int res = 0,s;
		for ( Entry<String, Type> iter : elements.entrySet()) {
			s = Math.max(4,iter.getValue().getRealSize(env));
			res += s;
		}
		return res;
	}
}
