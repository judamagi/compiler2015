package parser;

import java.util.Iterator;
import java.util.Map.Entry;

import error.SemanticError;
import Env.Env;
import IR.BinaryOperaterAssgin;
import IR.Instruction;
import IR.IntegerConstant;
import IR.Name;
import IR.Temp;

public final class UNION extends Record {
	
	public UNION(String name, Env subEnv) {
		this.name = name;
		elements = subEnv.tables;
	}
	
	@Override
	public String toString() {
		return  name;
	}

	@Override
	public boolean typeEquals(Type baseType) {
		if(baseType instanceof UNION) {
			return baseType == this;
		}
		return false;
	}

	@Override
	Name calcOffset(String identifier, Env env) throws SemanticError {
		return new IntegerConstant(0);
	}

	int __size = -1;
	@Override
	int calcSize(Env env) throws SemanticError {
		if(__size != -1) return __size;
		int res=4,s;
		for ( Entry<String, Type> iter : elements.entrySet()) {
			s = iter.getValue().calcSize(env);
			res = Math.max(res,s);
		}
		__size = res;
		return res;
	}

	@Override
	public
	int getConstSpace() throws SemanticError {
		int res=4,s;
		for ( Entry<String, Type> iter : elements.entrySet()) {
			s = iter.getValue().getConstSpace();
			res = Math.max(res,s);
		}
		return res;
	}

	@Override
	public int getRealSize(Env env) throws SemanticError {
		int res=4,s;
		for ( Entry<String, Type> iter : elements.entrySet()) {
			s = iter.getValue().getRealSize(env);
			res = Math.max(res,s);
		}
		return res;
	}
}
