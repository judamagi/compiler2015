package parser;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import error.LexicalError;
import error.ParseError;
import error.SemanticError;
import lexer.Lexer;
import lexer.Tag;

public class Parser {
	
	Lexer lexer;
	Map<String, Integer> BinOpPrecedence = new HashMap<String, Integer>();
	
	
	public Parser(Lexer lexer) {
		this.lexer = lexer;
		//BinOpPrecedence.put(".",new Integer(500000));
		//BinOpPrecedence.put("->",new Integer(500000));
		BinOpPrecedence.put("*",new Integer(100000));
		BinOpPrecedence.put("/",new Integer(100000));
		BinOpPrecedence.put("%",new Integer(100000));
		BinOpPrecedence.put("+",new Integer(50000));
		BinOpPrecedence.put("-",new Integer(50000));
		BinOpPrecedence.put("<<",new Integer(10000));
		BinOpPrecedence.put(">>",new Integer(10000));
		BinOpPrecedence.put(">",new Integer(5000));
		BinOpPrecedence.put(">=",new Integer(5000));
		BinOpPrecedence.put("<",new Integer(5000));
		BinOpPrecedence.put("<=",new Integer(5000));
		BinOpPrecedence.put("==",new Integer(4000));
		BinOpPrecedence.put("!=",new Integer(4000));
		BinOpPrecedence.put("&",new Integer(3000));
		BinOpPrecedence.put("^",new Integer(2000));
		BinOpPrecedence.put("|",new Integer(1000));
		BinOpPrecedence.put("&&",new Integer(500));
		BinOpPrecedence.put("||",new Integer(400));
	}
	
	
	public static final VOID VOID = new VOID();
	public static final INT INT = new INT();
	public static final CHAR CHAR = new CHAR();
	int counterAnonymous = 0;
	TypeProto TypeSpecifier() throws ParseError, LexicalError, SemanticError {
		if (lexer.currentTag != Tag.type) throw new ParseError("error type");
		if (lexer.IdentifierStr.equals("void")) {
			lexer.getTok();
			return new TypeProto("void");			
		}
		if (lexer.IdentifierStr.equals("int")) {
			lexer.getTok();
			return new TypeProto("int");
		}
		if (lexer.IdentifierStr.equals("char")) {
			lexer.getTok();
			return new TypeProto("char");
		}
		if (lexer.IdentifierStr.equals("struct") || lexer.IdentifierStr.equals("union")) {
			String typename = new String(lexer.IdentifierStr);
			RecordDecl tree=null;
			lexer.getTok();
			if (lexer.currentTag == Tag.identifier) {
				String identifierName = lexer.IdentifierStr;
				lexer.getTok();
				if(lexer.currentTag==Tag.OP && lexer.OpStr.equals("{")){
					lexer.getTok();//eat '{'
					tree = new RecordDecl(typename,identifierName);
					while(true) {
						tree.addRecord(parseRecordDeclaration());
						if(lexer.currentTag==Tag.OP && lexer.OpStr.equals("}")) break;							
					}
					lexer.getTok();// eat '}'
				}
				if(typename.equals("struct")) return new TypeProto("struct "+identifierName,tree); else return new TypeProto("union "+identifierName,tree);				
			} else {
				if(!(lexer.currentTag==Tag.OP && lexer.OpStr.equals("{")))
					throw new ParseError("lack of identifier");
				String AnonymousName = String.format("<<anonymous%d>>", counterAnonymous++);
				tree = new RecordDecl(typename,AnonymousName);
				lexer.getTok(); //eat '{'
				while(true) {
					tree.addRecord(parseRecordDeclaration());
//					lexer.getTok();
					if(lexer.currentTag==Tag.OP && lexer.OpStr.equals("}")) break;							
				}
				lexer.getTok();//eat '}'				
				if(typename.equals("struct")) return new TypeProto("struct "+AnonymousName,tree); else return new TypeProto("union "+AnonymousName,tree);		
			}
		}
		return null;
	}
	
	/*
	 * recordDeclaration 
	 * 		::= type identifierExpr (, identifierExpr)* ;
	 */
	ArrayList<Decl> parseRecordDeclaration() throws ParseError, LexicalError, SemanticError {
		ArrayList<Decl> res = new ArrayList<Decl>();
		TypeProto ty = TypeSpecifier();
		if(ty.decl != null) res.add(ty.decl);
		if(lexer.currentTag==Tag.OP && lexer.OpStr.equals(";")) {
			lexer.getTok();
		} else {
			while(true) {
				IdentifierExpr id = getIdentifier();
				res.add(new FieldDecl(ty.type, id));
				if(lexer.currentTag==Tag.OP && lexer.OpStr.equals(";")) {
					lexer.getTok();
					break;
				}
				if(!(lexer.currentTag==Tag.OP && lexer.OpStr.equals(",")))
						throw new ParseError("lack of '=',',' or ';'");
				lexer.getTok();//eat ','
			}
		}
		return res;
	}

	/*
	 * identifier 
	 * 		::= '*'* name
	 * 		::= '*'* name([])*
	 */
	IdentifierExpr getIdentifier() throws LexicalError, ParseError, SemanticError {	
		String str= "";
		while(lexer.currentTag == Tag.OP && lexer.OpStr.equals("*")){
			str+=lexer.OpStr;
			lexer.getTok();
		}
		if(lexer.currentTag != Tag.identifier)
			throw new ParseError("not identifier");
		str+=new String(lexer.IdentifierStr);
		lexer.getTok();
		if (lexer.currentTag == Tag.OP && lexer.OpStr.equals("[")) {
			IdentifierArray res = new IdentifierArray(str);
			while(true){				
				lexer.getTok();// eat '['
				res.add(parseExpressionStmt());
				lexer.getTok(); // eat ']'
				if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals("[")))
					return res;
			}
		} else {
			return new Identifier(str);
		}
	}
	
	/*
	 * identifierExpr
	 * 		::= identifier
	 * 		::= identifier '(' expression * ')'
	 */
	Stmt parseIdentifierExpr() throws LexicalError, ParseError, SemanticError {
		IdentifierExpr temp=getIdentifier();
		if (lexer.currentTag == Tag.OP && lexer.OpStr.equals("(")) {
			lexer.getTok();
			CallExpr res = new CallExpr(temp);
			if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals(")"))) {
				while(true) {
					Stmt arg = parseAssignExprssion();
					if(arg == null) return null;
					res.add(arg);
					if (lexer.currentTag == Tag.OP && lexer.OpStr.equals(")")) break;
					if (lexer.currentTag == Tag.OP && !lexer.OpStr.equals(","))
						throw new ParseError("Expected ')' or ',' in argument list");
					lexer.getTok();//eat ','
				}
			}
			lexer.getTok();
			return res;
		} else {
			Stmt res = new DeclRefExpr(temp.identifier);
			if(temp instanceof IdentifierArray) {
				for(int i=((IdentifierArray) temp).expression.size()-1;i>=0;i--) {
					res = new ArrayAddressOperator(res,((IdentifierArray) temp).expression.get(i));
				}
				return res;
			} else	return new DeclRefExpr(temp.identifier);
		}
	}
	
	int getTokPrecedence() {
		Integer res = BinOpPrecedence.get(lexer.OpStr);
		if (res==null) return -1;
		return res.intValue();
	}
	
	/*
	 * numberExpr ::= integer
	 */
	IntegerLiteral parseNumberExpr() throws LexicalError {
		IntegerLiteral res = new IntegerLiteral(lexer.NumVal);
		lexer.getTok();
		return res;
	}
	
	/*
	 * charExpr ::= char
	 */
	CharacterLiteral parseCharExpr() throws LexicalError {
		CharacterLiteral res = new CharacterLiteral(lexer.CharVal);
		lexer.getTok();
		return res;
	}
	
	/*
	 * stringExpr 
	 * 		::= string
	 * 		::= string[expression]		
	 */
	Stmt parseStringExpr() throws LexicalError, ParseError, SemanticError {
		StringLiteral res = new StringLiteral(lexer.StringStr);
		lexer.getTok();//eat '"'
		if(lexer.currentTag == Tag.OP && lexer.OpStr.equals("[")){
			lexer.getTok(); // eat '['
			Stmt temp = parseExpressionStmt();
			lexer.getTok(); // eat ']'
			return new ArrayAddressOperator(res, temp);
		} else 	return res;
	}
	
	/*
	 * parenExpr 
	 * 		::= '(' expression ')'
	 * 		::= '(' type ')' primary
	 */
	Stmt parseParenExpr() throws LexicalError, ParseError, SemanticError {
		lexer.getTok(); // eat '('
		if(lexer.currentTag == Tag.type) {
			TypeProto ty = TypeSpecifier();
			while(lexer.currentTag == Tag.OP && lexer.OpStr.equals("*")){
				lexer.getTok();
				ty.type = "*" + ty.type; 
			}
			lexer.getTok(); // eat ')'			
			ImplicitCastExpr res = new ImplicitCastExpr(ty.type,ParseUnaryPrimaryExpr());
			return res;
		}
		Stmt res = parseExpressionStmt();
		if (lexer.currentTag != Tag.OP || !lexer.OpStr.equals(")"))
			throw new ParseError("expected ')'");
		lexer.getTok();
		return res;
	}
	
	final String[] unaryOperators={"~","++","--","!","&","*","+","-","sizeof"};
	
	boolean isUnaryOperator(String op) {	
		return Arrays.asList(unaryOperators).contains(op.intern());
	}
	
	SizeofOperator parseSizeofOperator() throws ParseError, LexicalError, SemanticError {
		if(lexer.currentTag == Tag.OP && lexer.OpStr.equals("(")) {
			lexer.getTok(); // eat '('
			if(lexer.currentTag == Tag.type) {
				TypeProto ty = TypeSpecifier();
				String temp = new String(ty.type);
				while(lexer.currentTag == Tag.OP && lexer.OpStr.equals("*")) {
					temp = "*" + temp;
					lexer.getTok(); // eat '*'
				}
				lexer.getTok(); // eat ')'
				return new SizeofOperator(temp);
			} else {
				SizeofOperator res = new SizeofOperator(parseExpressionStmt());
				lexer.getTok(); // eat ')'
				return res;
			}
		} else {
			return new SizeofOperator(ParseUnaryPrimaryExpr());
		}
	}
	
	/*
	 * parseUnaryPrimaryExpr 
	 * 		::= unary primary
	 * 		::= sizeof expression
	 */
	Stmt ParseUnaryPrimaryExpr() throws ParseError, LexicalError, SemanticError {
		if(lexer.currentTag != Tag.OP) return parsePrimary();
		String temp = new String(lexer.OpStr);
		lexer.getTok();
		if(temp.equals("sizeof")) return parseSizeofOperator();
		Stmt res = parsePrimary();
		return new UnaryOperator(temp, res);
	}
	
	/*
	 * primary
	 *		::= unary primary 
	 * 		::= primarySecondLevel (suffix-operator | member-operator primary)*
	 */
	Stmt parsePrimary() throws ParseError, LexicalError, SemanticError {
		if (lexer.currentTag == Tag.OP && isUnaryOperator(lexer.OpStr)) return ParseUnaryPrimaryExpr();
		Stmt res = parsePrimarySecondLevel();
		while(true) {
			if(lexer.currentTag == Tag.OP && (lexer.OpStr.equals("++")|| lexer.OpStr.equals("--"))){
				res = new SuffixOperator(lexer.OpStr,res);
				lexer.getTok();
				continue;
			}
			if(lexer.currentTag == Tag.OP && (lexer.OpStr.equals(".") || lexer.OpStr.equals("->"))){
				String temp = lexer.OpStr;
				lexer.getTok();
				IdentifierExpr id = getIdentifier();
				if (id instanceof Identifier) {
					res = new MemberExpr(res,temp,id.identifier);
				} else {
					res = new MemberExpr(res,temp,id.identifier);
					IdentifierArray a = (IdentifierArray)id;
					for(int i=a.expression.size()-1;i>=0;i--){
						res = new ArrayAddressOperator(res, a.expression.get(i));
					}
				}
				continue;
			}
			if(lexer.currentTag == Tag.OP && lexer.OpStr.equals("[")) {
				lexer.getTok(); // eat '['				
				res = new ArrayAddressOperator(res, parseExpressionStmt());
				lexer.getTok(); // eat ']'
				continue;
			}
			break;
		}
		return res;
	}
	
	/*
	 * primarySecondLevel
	 * 		::= identifier
	 * 		::= literal
	 *		::= parenexpr
	 *		::= getchar()
	 *		::= malloc()
	 */
	Stmt parsePrimarySecondLevel() throws ParseError, LexicalError, SemanticError {
		
		if (lexer.currentTag == Tag.identifier) return parseIdentifierExpr();
		if (lexer.currentTag == Tag.number) return parseNumberExpr();
		if (lexer.currentTag == Tag.Char) return parseCharExpr();
		if (lexer.currentTag == Tag.string) return parseStringExpr();
		if (lexer.currentTag == Tag.OP && lexer.OpStr.equals("(")) return parseParenExpr();
		if (lexer.currentTag == Tag.keywords && lexer.IdentifierStr.equals("getchar")) return parseGetchar();
		if (lexer.currentTag == Tag.keywords && lexer.IdentifierStr.equals("malloc")) return parseMallocStmt();
		if (lexer.currentTag == Tag.keywords && lexer.IdentifierStr.equals("printf")) return parsePrintfStmt();
		throw new ParseError("unknown token when expecting an expression");
	}
	
	Stmt parseDefVar() throws LexicalError {
		String temp = lexer.IdentifierStr;
		lexer.getTok(); // eat identifier
		return new DeclRefExpr(temp);
	}

	Getchar parseGetchar() throws LexicalError, ParseError {
		lexer.getTok();// eat "getchar"
		if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals("(")))throw new ParseError("lack of '('");
		lexer.getTok();// eat '('
		if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals(")")))throw new ParseError("lack of ')'");
		lexer.getTok();// eat ')'
		return new Getchar();
	}

	/*
	 * arrayAssignExpr ::= '{' expression* '}'
	 */
	CompoundStmt parseArrayAssignExpr() throws LexicalError, ParseError, SemanticError {
		lexer.getTok();
		CompoundStmt res = new CompoundStmt();
		while(!(lexer.currentTag==Tag.OP && lexer.OpStr.equals("}"))){
			res.add(parseExpression());
			if(lexer.currentTag==Tag.OP && lexer.OpStr.equals("}")){
				lexer.getTok();
				break;
			}
			if(!(lexer.currentTag==Tag.OP && lexer.OpStr.equals(",")))
					throw new ParseError("except ',' or '}'");			
			lexer.getTok();//eat','
			if(lexer.currentTag==Tag.OP && lexer.OpStr.equals("}")){
				lexer.getTok();
				break;
			}
		}
		return res;
	}
	
	/*
	 * binOpRHS ::= ( operator primary)*
	 */
	Stmt parseBinOpRHS(int ExprPrec, Stmt LHS) throws ParseError, LexicalError, SemanticError {
		while(true) {
			int TokPrec = getTokPrecedence();
			if (TokPrec < ExprPrec)
				return LHS;
			String BinOp = lexer.OpStr;
			lexer.getTok();
			Stmt RHS = parsePrimary();
			int NextPrec = getTokPrecedence();
			if(TokPrec < NextPrec) {
				RHS = parseBinOpRHS(TokPrec + 1, RHS);				
			}
			LHS = new BinaryOperator(BinOp, LHS, RHS);
		}
	}
	
	/*
	 * expression
	 * 		::= primary binoprhs
	 * 		::= arrayAssignExpr
	 */
	Stmt parseExpression() throws LexicalError, ParseError, SemanticError {
		if(lexer.currentTag==Tag.OP && lexer.OpStr.equals("{")) return parseArrayAssignExpr();
		Stmt lhs = parsePrimary();
		return parseBinOpRHS(0, lhs);
	}
	
	/*
	 * parseDeclaration ::= type identifier ('=' expression)? (',' identifier ('=' expression)? ) ';'
	 */
	@SuppressWarnings("unused")
	ArrayList<Decl> parseDeclaration() throws ParseError, LexicalError, SemanticError {
		RecordDecl recoredDecl=null;
		TypeProto ty = TypeSpecifier();
		ArrayList<Decl> res = new ArrayList<Decl>();
		if(ty.decl != null) res.add(ty.decl);
		if(lexer.currentTag==Tag.OP && lexer.OpStr.equals(";")) {
			lexer.getTok();
		} else {
			while(true) {
				IdentifierExpr id = getIdentifier();
				if(lexer.currentTag==Tag.OP && lexer.OpStr.equals("=")){
					lexer.getTok(); //eat '='
					res.add(new VarDecl(ty.type,id,parseExpression()));
				} else {
					res.add(new VarDecl(ty.type, id));
				}
				if(lexer.currentTag==Tag.OP && lexer.OpStr.equals(";")) {
					lexer.getTok();
					break;
				}
				if(!(lexer.currentTag==Tag.OP && lexer.OpStr.equals(",")))
						throw new ParseError("lack of '=',',' or ';'");
				lexer.getTok();//eat ','
			}
		}
		return res;
	}
	
	/*
	 * function ::= (parameter*) compound-statement
	 */
	FunctionDecl parseFunction(String type, IdentifierExpr declarator) throws LexicalError, ParseError, SemanticError {
		ArrayList<ParamVarDecl> paramList= new ArrayList<ParamVarDecl>();
		lexer.getTok(); // eat '('
		IdentifierExpr paramName;
		TypeProto paramType;
		if(lexer.currentTag == Tag.OP && lexer.OpStr.equals(")")){
			lexer.getTok(); //eat ')'
		} else {
			while(true){
				paramType = TypeSpecifier();
				paramName = getIdentifier();
				paramList.add(new ParamVarDecl(paramType.type,paramName));
				if(lexer.currentTag == Tag.OP && lexer.OpStr.equals(")")) {
					lexer.getTok();
					break;
				}
				if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals(","))) 
					throw new ParseError("lack of ',' or ')'");
				lexer.getTok();// eat ','
			}
		}
		return new FunctionDecl(type, declarator, paramList, parseCompoundStmt());
	}
	
	/*
	 * compoundStmt ::= '{' statement* '}'
	 */
	CompoundStmt parseCompoundStmt() throws LexicalError, ParseError, SemanticError {
		CompoundStmt res = new CompoundStmt();
		lexer.getTok(); // eat' {'
		while(true) {
			if(lexer.currentTag == Tag.OP && lexer.OpStr.equals("}")) break;
			res.add(parseStatement());			
		}
		lexer.getTok();// eat '}'
		return res;
	}
	
	
	IfStmt parseIfStmt() throws LexicalError, ParseError, SemanticError {
		lexer.getTok(); //eat "if"
		lexer.getTok(); //eat '('
		Stmt expr1,expr2,expr3;
		expr1 = parseExpression();
		if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals(")")))
			throw new ParseError("lack of ')'");
		lexer.getTok(); //eat ')'
		expr2 = parseStatement();
		if(lexer.currentTag == Tag.keywords && lexer.IdentifierStr.equals("else")) {
			lexer.getTok(); // eat "else"
			expr3 = parseStatement();
			return new IfStmt(expr1,expr2,expr3);
		}
		return new IfStmt(expr1,expr2);
	}
	
	ForStmt parseForStmt() throws LexicalError, ParseError, SemanticError {
		lexer.getTok(); //eat "for"
		lexer.getTok(); //eat '('
		Stmt expr1=null,expr2=null,expr3=null,expr4=null;
		if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals(";")))
			expr1 = parseExpressionStmt();
		lexer.getTok(); //eat ';'
		if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals(";")))
			expr2 = parseExpressionStmt();
		lexer.getTok(); //eat ';'
		if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals(")")))
			expr3 = parseExpressionStmt();
		lexer.getTok(); //eat ')'
		expr4 = parseStatement();
		return new ForStmt(expr1,expr2,expr3,expr4);
	}
	
	WhileStmt parseWhileStmt() throws LexicalError, ParseError, SemanticError {
		lexer.getTok(); //eat "while"
		lexer.getTok(); //eat '('
		Stmt expr,statement;
		expr = parseExpressionStmt();
		lexer.getTok(); //eat ')'
		statement = parseStatement();
		return new WhileStmt(expr,statement);
	}
	
	ReturnStmt parseReturnStmt() throws LexicalError, ParseError, SemanticError {
		lexer.getTok(); //eat "return"
		Stmt expr = parseExpressionStmt();
		lexer.getTok(); //eat ';'
		return new ReturnStmt(expr);
	}
	
	PrintfStmt parsePrintfStmt() throws LexicalError, ParseError, SemanticError {
		lexer.getTok(); //eat "printf"
		lexer.getTok(); //eat '('
		String fotmat = lexer.StringStr;
		lexer.getTok(); // eat 'fotmat'
		ArrayList<Stmt> lists = new ArrayList<Stmt>();
		if(lexer.currentTag == Tag.OP && lexer.OpStr.equals(")")){
			lexer.getTok(); // eat ')'
			return new PrintfStmt(fotmat, lists);
		}
		lexer.getTok(); //eat ','		
		Stmt expr;
		while(true) {
			expr = parseAssignExprssion();
			lists.add(expr);
			if(lexer.currentTag == Tag.OP && lexer.OpStr.equals(")")) break;
			if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals(",")))
				throw new ParseError("lack of ',' or ')'");
			lexer.getTok(); //eat ','
		}
		lexer.getTok(); //eat ')'
		return new PrintfStmt(fotmat,lists);
	}
	
	MallocStmt parseMallocStmt() throws LexicalError, ParseError, SemanticError {
		lexer.getTok(); //eat "malloc"
		lexer.getTok(); //eat '('
		Stmt expr = parseExpressionStmt();
		lexer.getTok(); //eat ')'
		return new MallocStmt(expr);
	}
	
	BreakStmt parseBreakStmt() throws LexicalError {
		lexer.getTok(); //eat "break"
		lexer.getTok(); //eat ';'
		return new BreakStmt();
	}
	
	ContinueStmt parseContinueStmt() throws LexicalError {
		lexer.getTok(); //eat "continue"
		lexer.getTok(); //eat ';'
		return new ContinueStmt();
	}
	
	/*
	 * parseStatement
	 * 		::= if stmt
	 * 		::= for stmt
	 * 		::= while stmt
	 * 		::= declaration
	 * 		::= return stmt
	 * 		::= expressions
	 * 		::= compound-statement
	 * 		::= break stmt
	 * 		::= continue stmt
	 */
	Stmt parseStatement() throws ParseError, LexicalError, SemanticError {
		if(lexer.currentTag == Tag.type){
			return parseDeclarationStmt();
		}
		if(lexer.currentTag == Tag.keywords && lexer.IdentifierStr.equals("if")) {
			return parseIfStmt();
		}
		if(lexer.currentTag == Tag.keywords && lexer.IdentifierStr.equals("for")) {
			return parseForStmt();
		}
		if(lexer.currentTag == Tag.keywords && lexer.IdentifierStr.equals("while")) {
			return parseWhileStmt();
		}
		if(lexer.currentTag == Tag.keywords && lexer.IdentifierStr.equals("return")) {
			return parseReturnStmt();
		}
		if(lexer.currentTag == Tag.keywords && lexer.IdentifierStr.equals("break")) {
			return parseBreakStmt();
		}
		if(lexer.currentTag == Tag.keywords && lexer.IdentifierStr.equals("continue")) {
			return parseContinueStmt();
		}
		if(lexer.currentTag == Tag.OP && lexer.OpStr.equals("{")) {
			return parseCompoundStmt();
		}
		if(lexer.currentTag == Tag.OP && lexer.OpStr.equals(";")) {
			lexer.getTok(); // eat ';'
			return new DoNoThing();
		}
		Stmt res = parseExpressionStmt();
		lexer.getTok(); // eat ','
		return res;
	}
	
	
	/*
	 * expressionStmt
	 * 		::= assignment-expression (',' assignment-expression)* 
	 */
	Stmt parseExpressionStmt() throws ParseError, LexicalError, SemanticError {
		if(lexer.currentTag == Tag.OP && lexer.OpStr.equals(";"))return new DoNoThing();
		Stmt res,expr = parseAssignExprssion();
		if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals(","))){
			return expr;
		}			
		res = expr;
		while (true) {			
			lexer.getTok(); //eat ','
			expr = parseAssignExprssion();
			res = new BinaryOperator(",", res, expr);			
			if(lexer.currentTag == Tag.OP && (lexer.OpStr.equals(";") || lexer.OpStr.equals(")"))){
				break;
			}
			if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals(",")))
				throw new ParseError("lack of ',' or ';'");
		}
		return res;
	}
	
	/*
	 * assignExprssion
	 * 		::= unary-expression assignment-operator assignment-expression
	 * 		::= nomal expression
	 */
	Stmt parseAssignExprssion() throws ParseError, LexicalError, SemanticError {
		Stmt lhs = parsePrimary();
		if(lexer.currentTag == Tag.OP && isAssignOperator(lexer.OpStr)){
			String temp=lexer.OpStr;
			lexer.getTok(); //eat assignment-operator
			return new AssignOperator(lhs,temp,parseAssignExprssion());
		} else {
			return parseBinOpRHS(0, lhs);
		}
	}

	final String[] assignOperators={"=","*=","/=","%=","+=","-=","<<=",">>=","&=","^=","|="};

	boolean isAssignOperator(String opStr) {
		return Arrays.asList(assignOperators).contains(opStr.intern());
	}

	Stmt parseDeclarationStmt() throws ParseError, LexicalError, SemanticError {
		return new DeclStmt(parseDeclaration());
	}

	/*
	 * program
	 * 		::= declearation
	 * 		::= function-declearation
	 */
	public TranslationUnitDecl parseProgram() throws Exception {
		TranslationUnitDecl res = new TranslationUnitDecl();
		TypeProto type;
		try {
			lexer.getTok();
			while (lexer.currentTag != Tag.EOF) {
				try {
					type = TypeSpecifier();
					if(type.decl != null) res.addDecl(type.decl);
					if(lexer.currentTag == Tag.OP && lexer.OpStr.equals(";")){
						lexer.getTok(); //eat ';'
						continue;
					}
				} catch (Exception e) {
					System.err.println(e.getMessage());
					throw new ParseError("Illegal declaration or function-definition: not type-specifier");
				}
				IdentifierExpr declarator = getIdentifier();
				if(lexer.currentTag == Tag.OP && lexer.OpStr.equals("(")) {
					res.addDecl(parseFunction(type.type, declarator));
				} else {
					while(true) {
						if(lexer.currentTag == Tag.OP && (lexer.OpStr.equals("=")))	{
							lexer.getTok(); //eat '='
							res.addDecl(new VarDecl(type.type,declarator,parseExpression()));
						} else {
							res.addDecl(new VarDecl(type.type,declarator));
						}
						if(lexer.currentTag == Tag.OP && lexer.OpStr.equals(";")) {
							lexer.getTok();//eat ';'
							break;
						}
						if(!(lexer.currentTag == Tag.OP && lexer.OpStr.equals(","))) { 
							throw new ParseError("excepted ',' or ';'");
						} else lexer.getTok(); // eat ','
						
						declarator = getIdentifier();
					}
					
				}
			}
		} catch (Exception e) {
			System.err.printf("line %d: ", lexer.lineNumber);
			throw e;
		}
		
		return res;
	}
	
}
