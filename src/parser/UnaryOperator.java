package parser;

import Env.Env;
import IR.Assign;
import IR.BinaryOperaterAssgin;
import IR.Instruction;
import IR.IntegerConstant;
import IR.Name;
import IR.PointerWrite;
import IR.Temp;
import IR.UnaryOperatorAssign;
import IR.Variable;
import IR.getAddress;
import IR.solvePointer;
import error.SemanticError;

public class UnaryOperator extends Stmt {
	String Op;
	Stmt statement;
	public UnaryOperator(String temp, Stmt res) {
		// TODO Auto-generated constructor stub
		Op = temp;
		statement = res;
	}
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<UnaryOperator: %s>\n",blank,Op);
		statement.show(indent+1);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		annotated res = statement.check(env);
		if(Op.equals("*")){
			if(!(res.type instanceof Pointer) && !(res.type instanceof INT))throw new SemanticError("resolved reference is not a pointer or address");			
			Type x;
			if(res.type instanceof Pointer){
				x = ((Pointer)res.type).getRealType(env);
			} else {
				x = Parser.INT;
			}
			Name temp = new Temp();
			Instruction.add(new solvePointer(temp,res.name,x.calcSize(env)));
			return new annotated(x, true, temp);
		}
		if(Op.equals("&")){
			if(!res.isLeftValue)throw new SemanticError("error: lvalue required as unary '&' operand");
			Instruction Ins = Instruction.getLastInstruction();
			Name temp = new Temp();
			if(Ins instanceof solvePointer && ((solvePointer) Ins).x() == res.name) {
				Instruction.removeLastInstruction();
				return new annotated(Parser.INT, false, Ins.y());
			} else {
				Instruction.add(new getAddress(temp, res.name));
				return new annotated(Parser.INT, false,temp);
			}						
		}
		if(Op.equals("-")|| Op.equals("!") || Op.equals("~") || Op.equals("+")){			
			Type.matchAssign(Parser.INT, res.type);
			if(Op.equals("+"))return new annotated(Parser.INT, false, res.name);
			Variable temp = new Temp();
			Instruction.add(new UnaryOperatorAssign(temp, Op, res.name));
			return new annotated(Parser.INT,false,temp);
		}
		if(!res.isLeftValue) throw new SemanticError("no a left value");
		if(Op.equals("++") || Op.equals("--")) {
			Instruction lastInstruction = Instruction.getLastInstruction();
			Instruction.add(new BinaryOperaterAssgin((Variable) res.name, res.name, new IntegerConstant(1), Op.substring(0, 1)));
			if(lastInstruction instanceof solvePointer && ((solvePointer) lastInstruction).x() == res.name) {
				Name y = ((solvePointer)lastInstruction).y();
				Instruction.add(new PointerWrite(y, res.name,4));
			}
			return new annotated(res.type, false, res.name);
		}
		Variable temp = new Temp();
		Instruction.add(new UnaryOperatorAssign(temp, Op, res.name));
		return new annotated(res.type, false,temp);
	}
}
