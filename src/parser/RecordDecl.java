package parser;

import java.util.ArrayList;

import Env.Env;
import error.SemanticError;

public class RecordDecl extends Decl {
	String type; // "struct" or "union"
	String recordName;
	ArrayList<Decl> recordList = new ArrayList<Decl>();
	public RecordDecl(String typename, String identifierName) {
		// TODO Auto-generated constructor stub
		this.type = typename;
		recordName = new String(type+ " "+identifierName);
	}
	public void addRecord(ArrayList<Decl> decl) {
		for (Decl decl2 : decl) {
			recordList.add(decl2);
		}
	}
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<RecordDecl: %s %s>\n",blank,type,recordName);
		for (Decl decl : recordList) {
			decl.show(indent+1);
		}
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		if (env.structure.containsKey(recordName)) throw new SemanticError("redefined:"+recordName);
		Env subEnv = new Env(env,true,false);		
		for (Decl decl : recordList) {
			decl.check(subEnv);
		}
		Record baseType;
		if(type.equals("struct")) baseType=new STRUCT(recordName,subEnv);else baseType=new UNION(recordName,subEnv);
		env.structure.put(recordName, baseType);
		return null;
	}
}
