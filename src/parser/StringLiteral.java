package parser;

import java.util.ArrayList;

import Env.Env;
import IR.StringConstant;
import IR.__String__;
import error.SemanticError;

public class StringLiteral extends Literal {
	public StringLiteral(String stringStr) {
		stringLiteral = stringStr;
		StringConstant.addNewString(stringStr);
	}

	String stringLiteral;
	public void show(int indent) {
		String blank = "";
		for(int i=0;i<indent;i++)
			blank += '\t';
		System.err.printf("%s<StringLiteral: %s>\n",blank,stringLiteral);
	}
	@Override
	public annotated check(Env env) throws SemanticError {
		return new annotated(new Pointer("char"), false,new __String__(stringLiteral));
	}
	@Override
	public String getValue() {
		return stringLiteral.intern();
	}
}
